# Meedias

CMS budoucnosti. Tento dokument popisuje fungování a práci s centrálním serverem
projektu. Pro lehkost bytí také obsahuje status a rozebrání plánovaných funkcí celého
projektu.

Prvních několik sekcí se věnuje instalaci a správě projektu, následuje mapa repozitáře
a dokumentace, dále architektura projektu a databáze, limitace z hlediska deploymentu
na server a z hlediska vývojáře, ilustrační příklad implementace nového endpointu
a finálně status.

## Instalace
* Instalace Rustu z <https://www.rust-lang.org/en-US/install.html>
* Instalace Nimu z <https://nim-lang.org/>
* Otevření složky v příkazovém řádku
* `cargo run` pro rychlé spuštění
* `cargo run --release` rychlé spuštění release buildu
* `cargo build` pro vytvoření debug buildu ve složce target/debug
* `cargo build --release` pro vytvoření produkčního buildu ve složce target/release

Konfigurace webserveru je možná přes Rocket.toml,
viz <https://github.com/SergioBenitez/Rocket/blob/master/examples/config/Rocket.toml>

Dále je zapotřebí nainstalovat [conserve](https://github.com/sourcefrog/conserve)
pro zálohování databází.

## Testování
* Pro spouštění testů je potřeba vytvořit v Postgresu superuživatele test:test
* Testy běží na localhostu

Spustí se následujícím příkazem:
```bash
$ cargo test
```

## Terminologie a verze
V názvech může být nepořádek, tako krátka sekce slouží k alespoň částečnému vyjasnění.

- **remedias** - centrální server napsaný v Rustu, zahrnuje všechny součásti popsané v
  sekci *Architektura*
- **Meedias** - celý projekt, tj. server, front-end a koncepty
- **Front-end** - projekt napsaný ve Vue, artefakty vytvořené jeho sestavením je to,
  co si uživatelé nasadí na jejich webové servery
- **Memedias** - přetvořené PHP CMS Pico, které bude znova přetvořené na hezké klikátko
  pro instalaci front-endu na webserver.
- **Medias-old** - Vue front-end + Laravel backend
<br>

No a nyní k verzím, kterými projekt prošel:

- v1: **Medias-oldest** (pracovní název) - nejstarší známá verze, back-end napsaný v Node
	- nepochybně velmi originální název je neznámý
- v2: **Medias** - verze, kde je s Vue front-endem spárovaný Laravel back-end
	- název je pravopisná chyba, překlep
- v3: **Memedias** - přetvořené Pico CMS s vlastním front-endem. Tato verze bude nadále sloužit
  jako instalačka.
  - název je meme
- v4: **Remedias** - zatím poslední verze (momentálně Remedias má sám verzi v2.0.1), kde je
  nepočitatelně front-endů spojeno s jedním centrálním serverem napsaným v Rustu.
  - název je z anglického *remedy*
  - v1.x.x - normální monolitický server
  - v2.0.x - distribuovaný server s architekturou mikrokernelu popsanou níže

## Mapa repozitáře

	.
	├── .gitignore  - konfigurace gitu
	├── .gitmodules - uchovává informace o submodulech (front-end)
	├── build.rs    - pomocný sestavovací skript remediasu, automaticky spouští prvotní migraci
	├── Cargo.lock  - metadata o momentálně používaných knihovnách; společný pro celý projekt 
	├── Cargo.toml  - konfigurační soubor pro Cargo, definuje workspace, ale jinak se vztahuje jen na remedias 
	│
	├── api - obsahuje knihovny s jednotlivými endpointy, vždy lib.rs obsahuje controller a src/bin/ jednotlivé respondery
	│   ├── category - endpointy kategorií
	│   ├── content  - endpointy obsahu
	│   ├── instance - endpointy instancí
	│   ├── page     - endpointy stránek
	│   ├── query    - endpointy pro query
	│   ├── tags     - endpointy tagů
	│   └── user     - uživatelské endpointy
	│
	├── crate - obsahuje oddělené součásti projektu
	│   ├── ipc-rs    - knihovna pro meziprocesovou komunikaci pomocí SysV IPC protokolu
	│   ├── reapi     - obsahuje typy používané pro endpointy
	│   ├── redb      - všechno co se týká databáze: modely, schéma, připojování, konfigurace 
	│   ├── relog     - knihovna pro asynchrování logování ve formátu popsaném výše
	│   ├── remail    - odesílání emailů
	│   ├── restatic  - routy pro statické odesílání souborů, zobrazování dokumentace a landing page
	│   ├── reutils   - utility: autentifikace, porovnávání obsahu, připojení k databázi, existence uživatele
	│   ├── sentinel  - program sentinel (viz výše)
	│   └── README.md - README týkající se těchto crates
	│
	├── landing - obsahuje šablony pro landing-page
	│   ├── about.tera     - stránka 'O nás'
	│   ├── base.tera      - základní šablona, ostatní od ní dědí
	│   ├── contact.tera   - stránka kontaktů
	│   ├── landing.tera   - index webu
	│   ├── portfolio.html - portfólio (zatím nezpracováno) TODO
	│   ├── README.txt     - README týkající se šablony landing-cube
	│   └── services.html  - sloužby (zatím nezpracováno) TODO
	│
	├── misc - vedlejší soubory
	│   ├── bootstrap_reference - vygenerovaná 'reference' bootstrapu; je to spustitelný skript
	│   ├── endpoints           - referenční seznam endpointů
	│   └── status              - dokument o stavu projektu
	│
	├── nim - složka programu 'watchman', který je napsaný v Nimu
	│   ├── cli.nim          - zobrazovací modul, modifikovaná verze původně z balíčkovače Nimble
	│   ├── watchman.nim     - hlavní modul
	│   ├── watchman.nimble  - konfigurační soubor pro Nimble
	│   └── watchman.nim.cfg - konfigurační soubor pro hlavní modul, v součastnosti pouze zapíná multithreading
	│
	├── README.md   - tento soubor
	├── Rocket.toml - konfigurační soubor pro framework Rocket; tady se nastaví db pomocí klíčů z crate/redb/src/lib.rs
	├── sentinel    - spouštecí skript pro program sentinel
	│
	├── src - složka obsahující zdrojový kód kořenové crate
	│   ├── api - kód API endpointů (jen schránky mapující api/ crates)
	│   │   ├── category.rs - endpointy pro kategorie (a zároveň modifikace pole kategorií u obsahu)
	│   │   ├── content.rs  - endpointy pro obsah
	│   │   ├── instance.rs - endpointy pro instance
	│   │   ├── mod.rs      - kořenový soubor modulu, reexportuje symboly z crate/reapi
	│   │   ├── page.rs     - endpointy pro vue-grid fragmenty
	│   │   ├── query.rs    - endpointy pro vyhledávání obsahu
	│   │   ├── tags.rs     - endpointy pro práci s tagy, jak u instancí tak u obsahu
	│   │   └── user.rs     - endpointy pro všechno, co se týká uživatelů
	│   │
	│   ├── bin - složka obsahující vedlejší spustitelné soubory crate
	│   │   └── sentinel.rs - program sentinel (stará verze, dočasný)
	│   │
	│   ├── lib.rs - hlavní modul projektu
	│   └── main.rs - modul spustitelného souboru 'remedias' (protože remedias je de facto jedna velká knihovna)
	│
	├── src-front - front-endový submodul
	│
	└── static - staticky odeslané soubory
	    ├── css - kaskádové styly jak pro front-end tak pro landing
	    │   ├── animate.css               - Animate <http://daneden.me/animate>
	    │   ├── bootstrap.css             - Bootstrap verze 3.3.5, pro landing
	    │   ├── bootstrap.min.css         - Bootstrap verze 4.0.0, pro front-end
	    │   ├── flexslider.css            - jQuery FlexSlider <http://www.woothemes.com/flexslider>
	    │   ├── font-awesome.min.css      - FontAwesome       <https://fontawesome.io>
	    │   ├── icomoon.css               - IcoMoon           <https://icomoon.io/app/>
	    │   ├── magnific-popup.css        - Magnific Popup    <http://dimsemenov.com/plugins/magnific-popup/>
	    │   ├── owl.carousel.min.css      - Owl Carousel
	    │   ├── owl.theme.default.min.css - Owl Theme
	    │   ├── style.css                 - kaskádové styly landing šablony
	    │   └── themify-icons.css         - Themify Icons
	    │
	    ├── fonts - fonty pro jednotlivé frameworky
	    │   ├── bootstrap      - Bootstrap fonty
	    │   ├── icomoon        - IcoMoon fonty
	    │   └── themify-icons  - Themify Icons fonty
	    │
	    ├── images - obrázky
	    │   ├── bootstrap.png     - logo Bootstrapu
	    │   ├── client_1.png      - placeholdry obrázků spokojených klientů
	    │   ├── client_2.png      ^
	    │   ├── client_3.png      ^
	    │   ├── client_4.png      ^
	    │   ├── client_5.png      ^
	    │   ├── cube.png          - staré logo landing šablony
	    │   ├── img_md_1.jpg      - pozadí šabolny
	    │   ├── img_1.jpg         - placeholdry pro produkty (šablona portfolio)
	    │   ├── img_2.jpg         ^
	    │   ├── img_3.jpg         ^
	    │   ├── img_4.jpg         ^
	    │   ├── loader.gif        - načítací kolečko
	    │   ├── loc.png           - pin na označení lokace na mapě
	    │   ├── logo.png          - kostička (staré logo)
	    │   ├── magnusi.png       - :^)
	    │   ├── meedias_logo.png  - naše logo
	    │   ├── person_1.jpg      - placeholdery pro členy týmu
	    │   ├── person_2.jpg      ^
	    │   ├── person_3.jpg      ^
	    │   ├── pico.png          - logo Pico CMS 
	    │   ├── rocket.png        - logo frameworku Rocket
	    │   ├── staff_1.jpg       - placeholdery pro zaměstnance (nevyužité)
	    │   ├── staff_2.jpg       ^
	    │   ├── staff_3.jpg       ^
	    │   └── 1457911811183.png - :^)
	    │
	    └── js - javascriptové frameworky
	        ├── bootstrap.js                 - Bootstrap verze 3.3.5, pro landing
	        ├── bootstrap.min.js             - Bootstrap verze 4.0.0, pro front-end
	        ├── google_map.js                - Google Maps API
	        ├── jquery.countTo.js            - jQuery plugin countTo
	        ├── jquery.easing.1.3.js         - jQuery plugin pro easing
	        ├── jquery.magnific-popup.min.js - jQuery Magnific Popup
	        ├── jquery.min.js                - jQuery, pro landing
	        ├── jquery.slim.js               - jQuery, pro front-end
	        ├── jquery.stellar.min.js        - jQuery Stellar
	        ├── jquery.waypoints.min.js      - jQuery Waypoints
	        ├── magnific-popup-options.js    - konfigurace pro Magnific Popup
	        ├── main.js                      - hlavní skript landingu
	        ├── modernizr-2.6.2.min.js       - Modernizr framework
	        ├── owl.carousel.min.js          - Owl Carousel
	        └── respond.min.js               - Respond

## Endpointy
V současnosti Remedias API poskytuje okolo 70 endpointů rozdělených do těchto kategorií:

* uživatelské - pro správu uživatelů
* vyhledávací - pro získávání, hledání a řazení obsahu
* stránkové - pro umožnění kouzelných mřížek vue-grid
* obsahové - pro práci s obsahem (články atd.)
* kategorií - pro správu kategorií (jejich přiřazování, hierarchie)
* instancí - pro práci s instancemi (vytváření, mázání, metadata)
<br>

Zpravidla je preferován takový model, kde každý požadavek vrací málo dat (například jenom ID
kategorií místo seznamu kompletních záznamů) namísto toho, aby server poslal všechno jako
jednu velkou odpověď.

Ano, někdy sice dojde k přenosu většího objemu dat, když se třeba klient (prohlížeč) ptá na
články jeden po jednom, ale zase na druhou stranu to má spoustu výhod:

1. **Paralelizace** - klient může poslat X požadavků najednou a server je paralelně zpracovávat,
   to všechno zrychluje a lépe dělí práci mezi jádra procesoru. Je například mnohem efektnější,
   když server odešle čtyři články najednou, místo toho, aby jel sériově.
2. **Úspornost** - například pro aktualizaci jména uživatele stačí odeslat jen nové jméno (plus
   autentifikační údaje), místo celého záznamu
3. **Nároky na paměť** - Pokud odesíláme tisíc záznamů, tak pokud je to jeden po jednom, nebude
   v paměti viset 999 záznamů než načteme ten poslední. To se hodí pro podmínky, kdy je server
   omezený operační pamětí, která je k dispozici. Navíc, malá data se dají uložit na stack
   nebo dokonce do registru a procesor s nimi pracuje výrazně rychleji
4. **Bezpečnost přenosu** - kdyby došlu k přerušení toku dat, nezbývalo by klientovi než si znovu
   říci o tisíc záznamů, takto jde jednoduše pokračovat tam, kde byl přenos přerušen
5. **Bezpečnost jako taková** - malé požadavky, málo kódu, čím víc kódu, tím víc komplexity,
   čím víc komplexity, tím víc se toho může podělat
<br>

Seznam endpointů je v souboru `endpoints` v kořenovém adresáři repozitáře.

## Dokumentace
Server a všechny jeho části jsou kompletně zdokumentovány v češtině, s výjimkou `ipc-rs`,
což je obecná open-source knihovna pro mezi-procesovou komunikaci. Projekt je nastaven tak,
že chybějící dokumentace u věřejných symbolů jak v knihovnách tak v programech je považována
za kompilační chybu.

Na rozdíl od dokumentace jsou komentáře v kódu a zprávy gitových revizí v angličtině. Do
budoucna je v plánu přeložit dokumentaci také i do angličtiny.

Dokumentaci jde vygenerovat a otevřít buď pomocí `cargo doc --all --open` nebo použitím
přiloženého programu `sentinel`, viz jeho sekce.

#### TODO:  
[\_] dokumentace front-endu (je to vůbec potřeba?)  
[\_] dokumentace responderů (nízká priorita)  
[\_] přidání vzorků JSONu k dokumentaci každého endpointu (střední priorita)  


## Limitace pro deployment
Samozřejmě, je potřeba aby stroj na kterém Remedias poběží používal nějakou
distribuci Linuxu, preferován je Arch, i když jediným požadavkem je splnění
podmínek popsaných na začátku tohoto dokumentu plus základní unix utilitky.
A pochopitelně POSIXový shell, tj. `sh`, `bash`, `dash` nebo `ksh`.

Z hlediska systémových prostředků je pro sestavení release buildu potřeba
alespoň gigabajt RAM paměti, dva gigabajty volného prostoru a spousta
trpělivosti. Výsledné soubory mají však jen zhruba 370 megabajtů.

Délka kompilace je přibližně dvojnásobná u release buildu, na mém počítači
přibližně patnáct minut pro celý projekt (což už je dostatečný důvod
pro fragmentaci kompilace a mikroservery).

Co se týče běhu, tak potenciální limity představují velikost stacku,
limit počtu zpráv ve frontě a limit velikosti zpráv. Standardní velikost
stacku je ovšem dostatečně velká, abychom zvládli několik tisíc požadavků
najednou, proto tady rozhodně nebudou problémy.

Standardní limit počtu zpráv činí 32 tisíc, což by bylo zaplněno při
necelých 32 tisících současných požadavků, takže tady je to taky v cajku.

Standardní limit velikosti správy ovšem činí jen 8KB, což je sice velkorysé
pro většinu dat, ale s budoucím nástupem velkých instancí bude možná
potřeba jej zvýšit. To se dělá editací souboru **/proc/sys/kernel/msgmax**,
do kterého se zadá nový limit v bajtech. K navýšení tohoto limitu je potřeba
*root* uživatel.

## Příklad: implementace nového endpointu
TODO

## Status
Tato sekce rozebírá status projektu podle dokumentu zaslaného Micha(i)lem
'*Výsledky schůze ohledně funcí CMS projektu VÝŠ 18. srpna 2017 - progress k
10.04.18*'

### Obecné cíly (správně cíle)

- poskytování nástrojů pro:
	- tvorbu obsahu
		* **Stav:** technicky vzato funkční
		* **Zbývá:**: dodělat front-end, zabývat se kouzly s HTML ve wordu
		* **Priorita:**: velká
	- organizování spolupráce na tvorbě obsahu
		* **Stav:** neexistuje
		* **Požadavky:**: endpointy pro implementaci variace na <strike>večerní téma</strike> na Asanu/Kanboard,
		             patričné komponenty na front-endu
		* **Zbývá:**: všechno
		* **Priorita:**: středně-velká

- správa obsahu:
	- ukládání
		* **Stav:** plně funkční
		* **Zbývá:**: využití konceptů a koše na front-endu
		* **Priorita:**: střední
	- správa verzí
		* **Stav:** funkční
		* **Zbývá:**: zobrazení řetězce změn na front-endu, technomagie k vracení verzí
		* **Priorita:**: malá
	- řízení toku dat, dokumentů atd.
		* **Co to je?**

- publikování obsahu?
	* **Stav:** plně funkční
	* málo specifikované

- prezentace informací ve formě, které je vhodná pro navigaci, vyhledávání
	* **Stav:** back-end -> funkční, front-end -> minimálně
	* **Zbývá:**: vyřešit problémy s front-endem, implementace endpointů pro pořádné vyhledávání
	* **Priorita:**: střední
<br>

##### Modulární architektura: 2 moduly (vzhled a logika).
- tento výrok buď nedává smysl nebo je produktem slavné postavy *Captain Obvious*
<br><br>

- dynamické menu:
	* **Stav:** umožněno na back-endu, chybí Vue komponenta
	* **Zbývá:**: ta komponenta
	* **Priorita:**: poměrně velká

- blog:
	* **Stav:** funkční v závislosti na jiných funkcních
	* **Zbývá:**: dodělat front-end
	* **Požadavky:**: menu, feed, vyhledávání, řazení podle data
	* **Priorita:**: bude hotovo samo od sebe až budou kompletně splněny požadavky

- zprávy:
	* **Stav:** funkční v závislosti na jiných funkcních
	* **Zbývá:**: dodělat front-end
	* **Požadavky:**: menu, feed, vyhledávání, řazení podle data, preview a sexy obrázky
	* **Priorita:**: bude hotovo samo od sebe až budou kompletně splněny požadavky

- průzkumy veřejného mínění
	* **Stav:** vyžaduje diskuzi

- vyhledávání na webu
	* **Stav:** částečně funkční
	* **Zbývá:**: další endpointy pro vyhledávání a komponenty, které je využívají
	* **Priorita:**: je to další na seznamu

- statistiky návštěv
	* **Stav:** neexistuje
	* **Zbývá:**: triviální implementace
	* **Priorita:**: mikroskopická

### Specifikace funkcí
#### Správa obsahu
- CMS má umožňovat:
	- vytvářet
		* **Stav:** plně funkční až na to, jak blbne vue-grid
		* **Zbývá:**: odsoudit vue-grid
		* **Priorita:**: velká
	- mazat
		* **Stav:** plně funkční
		* **Zbývá:**: čudlík v editoru
		* **Priorita:**: středně-malá
	- ukládat do koše
		* **Stav:** teoreticky funkční
		* **Zbývá:**: front-end
		* **Priorita:**: střední
	- uspořádat hierarchicky stránky webu
		* **Stav:** plně funkční, hierarchie je iluze

- CMS má automaticky generovat snadnou navigaci na webu
	- viz vyhledávání a dynamické menu

- CMS má zaplňovat stránky webu obsah bloků různých typů
	* **Stav:** vue-grid...
	- text:
		* **Stav:** plně funkční
	- obrázek:
		* **Stav:** skoro funkční
		* **Zbývá:**: CSS
		* **Priorita:**: malá
	- seznam:
		* **Stav:** jen manuálně
		* **Zbývá:**: nemanuálně?
		* **Priorita:**: střední
	- tabulky:
		* **Stav:** funguje jen přes HTML (brzy snad i Markdown)
		* **Zbývá:**: Markdown
		* **Priorita:**: střední
	- atd.
		* **Stav:** neexistuje

#### Správa prezentace dat na webu
- Vytvoření šablony na vlastním jazyce
	* **Stav:** vyžaduje diskuzi

#### Řízení přistupu k datům
- sdílení přístupu administrátorů do různých částí webz
	* **Stav:** plně funkční
	* **Zbývá:**: grafické rozhraní
	* **Priorita:**: to příjde samo
- spojení řetězců autorů
	* **Stav:** jestli platí definice z poslední schůze tak plně funkční
	* viz **správa verzí**
- O**mezení** přístupu k některým stránkám, případě jejich **uzamčení**
  a zařazení do placeného obsahu
  * **Stav:** teoreticky funkční, ale placený obsah neexistuje
  * **Zbývá:**: udělat finance
  * **Priorita:**: velká, časová náročnost taky

#### Správa předplatného pošty
- CMS má vytvářet seznamy adres
	* **Stav:** funkční, ale nijak nevyužité
	* **Zbývá:**: grafické rozhraní
	* **Priorita:**: malá
- CMS má vytvářet a uprovovat dopisy
	* **Stav:** teoreticky funkční
	* **Zbývá:**: editor
	* **Priorita:**: malá
- CMS má posílat emaily
	* **Stav:** plně funkční
	* **Zbývá:**: nastavit obsah defaultních zpráv na něco jinýho než Bédu Hudečka,
	         viz *crate/remail/lib.rs*
	* **Priorita:**: malá

#### Správa hlaviček a bannerů
- kontrola názvu a meta t**a**gů (klíčová slova a popis)
	* **Stav:** popis neexistuje, globální tagy ano
	* **Zbývá:**: triviální implementace
	* **Priorita:**: velmi malá

### Funkce převzaté jiným CMS
- in-kontextový editor, který umožňuje provádět změny
  přímo stránce
  * **Stav:** stránky mají mřížkový editor, který by se dal de facto nazvat in-kontextový,
          články ne
  * **Zbývá:**: editor
  * **Priorita:**: střední
- integrovaný správce odkazů
	* **Stav:** vyžaduje diskuzi
- integrovaná galerie médií, správa obrázků a jejich základní editace přímo v redakčním
  systému, automatické vytváření miniatur definovaných rozměrů
  * **Stav:** neexistuje, editace vyžaduje diskusi, miniatury v podstatě existují
  * **Zbývá:**: všechno
  * **Priorita:**: středně-velká
- struktura trvalých odkazů přátelská k internetovým vyhledávačům a uživatelsky nastavitelná
	* **Stav:** struktura je trvalá, uživatelská nastavitelnost vyžaduje diskuzi
	* **Zbývá:**: nastavitelnost
	* **Priorita:**: středně-malá
- podpora pluginů pro rozšíření funkcí
	* **Stav:** z naší strany pluginy (tj. komponenty) jsou plně podporovány,
	        uživatelské jsou na tom hůř
  * **Zbývá:**: diskuze, jak a jestli to je vůbec možné, realizace
  * **Priorita:**: malá
- podpora témat vzhledu
	* **Stav:** plně funkční přes `./style.css`
	* **Zbývá:**: specifikace CSS tříd pro možnost konkrétnějšího přizpůsobení
	* **Priorita:**: středně malá
- podpora funkčních bloků - widgetů (např. poslední příspěvky, vlastní text,
  výpis RSS, atd.)
  * **Stav:** podpora existuje, všechny jmenované widgety ne
  * **Zbývá:**: udělat ty widgety
  * **Priorita:**: středně-velká
- možnost zařazovat příspěvky to kategorií (i více najednou)
	* **Stav:** plně funkční
	* **Zbývá:**: front-end
	* Priota: střední
- možnost přidávat příspěvkům tagy
	* **Stav:** plně funkční
	* **Zbývá:**: navigace pomocí tagů na front-endu
	* **Priorita:**: střední
- lze vytvářet hierarchii (strom) stránek
	* **Stav:** plně funkční, hierarchie je iluze
	* **Zbývá:**: zobrazování těchto 'stromů'
	* **Priorita:**: malá
- vyhledávání v rámci webových stránek
	* **Stav:** Ctrl+F funguje vždycky, viz vyhledávání na webu
- podpora trackback a pingback
	* **Stav:** neexistuje
	* **Zbývá:**: všechno
	* **Priorita:**: malá
- typografický filtr pro formátování a styl textu
	* **Stav:** viz editor
	* **Priorita:**: malá
- podpora více uživatelských účtů s odlišnými oprávněními
	* **Stav:** plně funkční
	* **Zbývá:**: udělat patřičné dashboardy pro jednoduché ovládání oprávnění
	* **Priorita:**: střední
- indexace (indexování) stránek
	* **Stav:** vyžaduje diskuzi
- tisknutelné verze stránek
	* **Stav:** neexistuje, vyžaduje studium
	* **Priorita:**: středně-malá
- lokalizace
	* **Stav:** neexistuje
	* **Zbývá:**: to vymyslet
	* **Priorita:**: malá
- vícejazyčné verze
	* **Stav:** viz lokalizace
- každý, kdo umí CSS, si může vytvořit vlastní """""layout"""""
	* **Stav:** plně funkční
	* **Zbývá:**: nic, maximálně tak návod
	* **Priorita:**: v podstatě žádná
- možnost naplánování obsahu na určité datum a čas
	* **Stav:** teoreticky funkční
	* **Zbývá:**: front-end
	* **Priorita:**: střední
- verifikace přihlášení
	* **Stav:** plně funkční
	* **Zbývá:**: jen Authy
	* **Priorita:**: středně-velká
- mobilní verze
	* **Stav:** pro stránky bez mřížek pevně funkční
	* **Zbývá:**: odsoudit vue-grid
	* **Priorita:**: ono to příjde samo

### Doplňující funkce
- kanboard
	* **Stav:** neexistuje
	* **Zbývá:**: udělat kanboard
	* **Priorita:**: podle mě středně malá, ale asi velká
- systém zprostředkovávání reklamy
	* **Stav:** neexistuje, jen koncept
	* **Zbývá:**: vymyslet to a zrealizovat
	* **Priorita:**: velká
- realizace backupů
	* **Stav:** N/A
	* Jedině potřebujeme dělat zálohy databáze, uživatel nemá co zálohovat
- možnost zapnout placenou blokací reklamy
	* **Stav:** věta nedává smysl, ale nepochybně neexistuje
	* **Stav:** viz systém reklam
