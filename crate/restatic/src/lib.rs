//! Tato knihovna obahuje veškeré routy
//! týkající se statického podávání souborů
//! a landing stránky
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate yansi;

extern crate reapi;

use rocket::Route;

pub mod landing;
pub mod static_server;

pub use crate::landing::{hmm, about, index, contacts, services, send};

pub use crate::static_server::{docs, handle, docs_redirect};

/// returns routes pt1
pub fn static_routes() -> Vec<Route> {
	routes![
		static_server::docs,
		static_server::handle,
		static_server::docs_redirect,
		static_server::handle_js,
		static_server::handle_img,
		static_server::handle_css,
		static_server::handle_fonts,
		static_server::handle_service,
	]
}

/// routes pt2
pub fn landing_routes() -> Vec<Route> {
	routes![
		landing::hmm,
		landing::send,
		landing::about,
		landing::index,
		landing::services,
		landing::contacts,
	]
}
