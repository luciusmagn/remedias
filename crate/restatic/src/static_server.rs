//! Tento miniaturní modul obsahuje responder pro
//! statické posílání souborů.
//! Díky garancím frameworku Rocket je zcela bezpečný a nejde
//! ošálit požadavky na `../` atd.

use std::path::{PathBuf, Path};

use yansi::Paint;
use rocket::response::Redirect;
use rocket::response::NamedFile;

/// Route posílající statické soubory
#[get("/static/<file..>", rank = 0)]
pub fn handle(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("static/").join(file)).ok()
}

/// Route posílající statické soubory
#[get("/js/<file..>", rank = 0)]
pub fn handle_js(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("static/js/").join(file)).ok()
}

/// Route posílající statické soubory
#[get("/img/<file..>", rank = 0)]
pub fn handle_img(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("static/img/").join(file)).ok()
}

/// Route posílající statické soubory
#[get("/fonts/<file..>", rank = 0)]
pub fn handle_fonts(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("static/fonts/").join(file)).ok()
}

/// Route posílající statické soubory
#[get("/css/<file..>", rank = 0)]
pub fn handle_css(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("static/css/").join(file)).ok()
}

/// Route posílající statické soubory
#[get("/service-worker.js", rank = 0)]
pub fn handle_service() -> Option<NamedFile> {
	NamedFile::open(Path::new("static/service-worker.js")).ok()
}

/// Statická routa pro posílání dokumentace
#[get("/docs/<file..>", rank = 4)]
pub fn docs(file: PathBuf) -> Option<NamedFile> {
	println!("    => {}", Paint::green(file.to_str().unwrap()));
	NamedFile::open(Path::new("target/doc/").join(file)).ok()
}

/// Užitečné přesměrování `/docs -> /docs/remedias/index.html`
#[get("/docs")]
pub fn docs_redirect() -> Redirect {
	Redirect::to("/docs/remedias/index.html")
}
