//! Modul obsahující routy týkající se landing page
use rocket_contrib::templates::Template;
use std::collections::HashMap;

use reapi::InputString;
//use remail::send_message;

use std::path::Path;
use rocket::response::NamedFile;

/// Index
#[get("/")]
pub fn index() -> Option<NamedFile> {
	NamedFile::open(Path::new("static/index.html")).ok()
}

/// Stránka s kontatky
#[get("/kontakty")]
pub fn contacts() -> Template {
	let context: HashMap<String, String> = HashMap::new();
	Template::render("contact", &context)
}

/// Hmm
#[get("/hmm")]
pub fn hmm() -> &'static str {
	"hmm..."
}

/// O nás
#[get("/o-nas")]
pub fn about() -> Template {
	let context: HashMap<String, String> = HashMap::new();
	Template::render("about", &context)
}

/// O nás
#[get("/sluzby")]
pub fn services() -> Template {
	let context: HashMap<String, String> = HashMap::new();
	Template::render("services", &context)
}

/// Random odesílač emailů
#[post(
	"/send/<_whe>/<_whom>/<_subject>",
	format = "application/json",
	data = "<_input>",
	rank = 4
)]
pub fn send(
	_whe: String,
	_whom: String,
	_subject: String,
	_input: InputString,
) -> String {
	//match send_message(&input.into_inner(), &subject, (&whe, &whom)) {
	//	Ok(_) => "success",
	//	Err(_) => "error",
	//}.to_string()
	"service temporarily unavailable".to_string()
}
