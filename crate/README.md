Tato složka obsahuje jednotlivé podknihovny serveru Remedias:

* `relog` - logování
* `reapi` - typy využívané pro api endpointy
* `redb` - komunikace s databází
* `remail` - odesílání emailů
* `reutils` - utilitní funkce
* `restatic` - věci týkající se statického serveru a landing stránky
* `ipc-rs` - knihovna pro mezi-procesovou komunikaci
* `sentinel` - program pro správu projektu Remedias

A dále mikroservery jednotlivých endpointů
