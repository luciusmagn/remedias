//! contains graphql roots and mutations
use uuid::Uuid;
use crate::Database;

use crate::models::*;
use crate::auth::{JwtContext, reason};

use juniper::RootNode;

use std::cmp;

pub mod ad;
pub mod role;
pub mod task;
pub mod user;
pub mod group;
pub mod content;
pub mod instance;
pub mod category;

/// Ad root
pub struct AdRoot;

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl AdRoot {
	/// fetch object by id
	pub fn ad_with_id(id: Uuid) -> Option<Ad> {
		let ads = Database::<Ad>::open().unwrap();

		ads.read().get(id)
	}
}

/// schema alias
pub type AdSchema = RootNode<'static, AdRoot, ad::AdMutations>;

/// Role root
pub struct RoleRoot;

impl RoleRoot {
	/// fetch object by id
	pub fn role_with_id(id: Uuid) -> Option<Role> {
		let roles = Database::<Role>::open().unwrap();

		roles.read().get(id)
	}

	/// fetch roles with instance
	pub fn role_with_instance(instance_id: Uuid) -> Vec<Role> {
		Database::<Role>::open()
			.unwrap()
			.read()
			.iter()
			.filter(|(_, x)| x.instance == instance_id)
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl RoleRoot {
	/// fetch object by id
	pub fn role_with_id(id: Uuid) -> Option<Role> {
		Self::role_with_id(id)
	}

	/// fetch roles with instance
	pub fn role_with_instance(instance_id: Uuid) -> Vec<Role> {
		Self::role_with_instance(instance_id)
	}
}

/// schema alias
pub type RoleSchema = RootNode<'static, RoleRoot, role::RoleMutations>;

/// Task root
pub struct TaskRoot;

impl TaskRoot {
	/// fetch object by id
	pub fn task_with_id(id: Uuid) -> Option<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks.read().get(id)
	}

	/// fetch task with name
	pub fn task_with_title(title: String, fuzzy: Option<bool>) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(title.as_str());

		tasks
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.title)
				} else {
					x.title.contains(&title)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch task of instance
	pub fn task_with_instance(id: Uuid) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks.read().iter().filter(|(_, x)| x.instance == id).map(|(_, x)| x).collect()
	}

	/// fetch task with creator
	pub fn task_with_creator(id: Uuid) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks.read().iter().filter(|(_, x)| x.creator_id == id).map(|(_, x)| x).collect()
	}

	/// fetch task with assignee
	pub fn task_with_assignee(id: Uuid) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.assignee_id == Some(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch task with reviewer
	pub fn task_with_reviewer(id: Uuid) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.reviewer_id == Some(id))
			.map(|(_, x)| x)
			.collect()
	}
}
#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl TaskRoot {
	/// fetch object by id
	pub fn task_with_id(id: Uuid) -> Option<Task> {
		Self::task_with_id(id)
	}

	/// fetch task with name
	pub fn task_with_title(title: String, fuzzy: Option<bool>) -> Vec<Task> {
		Self::task_with_title(title, fuzzy)
	}

	/// fetch task of instance
	pub fn task_with_instance(id: Uuid) -> Vec<Task> {
		Self::task_with_instance(id)
	}

	/// fetch task with creator
	pub fn task_with_creator(id: Uuid) -> Vec<Task> {
		Self::task_with_creator(id)
	}

	/// fetch task with assignee
	pub fn task_with_assignee(id: Uuid) -> Vec<Task> {
		Self::task_with_assignee(id)
	}

	/// fetch task with reviewer
	pub fn task_with_reviewer(id: Uuid) -> Vec<Task> {
		Self::task_with_reviewer(id)
	}
}

/// schema alias
pub type TaskSchema = RootNode<'static, TaskRoot, task::TaskMutations>;

/// User root
pub struct UserRoot;

impl UserRoot {
	/// fetch object by id
	pub fn user_with_id(id: Uuid) -> Option<User> {
		let users = Database::<User>::open().unwrap();

		users.read().get(id)
	}

	/// fetch user with instance
	pub fn user_with_instance(id: Uuid) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter(|(_, x)| x.sites.contains(&id))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch user with display name
	pub fn user_with_display(display: String, fuzzy: Option<bool>) -> Vec<User> {
		let users = Database::<User>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(display.as_str());

		users
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.display)
				} else {
					x.display.contains(&display)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch user with nick
	pub fn user_with_nick(nick: String, fuzzy: Option<bool>) -> Vec<User> {
		let users = Database::<User>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(nick.as_str());

		users
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.nick)
				} else {
					x.nick.contains(&nick)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch user with email
	pub fn user_with_email(email: String) -> Option<User> {
		Database::<User>::open()
			.unwrap()
			.read()
			.iter()
			.map(|(_, x)| x)
			.find(|x| x.email == email)
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl UserRoot {
	/// fetch object by id
	pub fn user_with_id(id: Uuid) -> Option<User> {
		Self::user_with_id(id)
	}

	/// fetch user with instance
	pub fn user_with_instance(id: Uuid) -> Vec<User> {
		Self::user_with_instance(id)
	}

	/// fetch user with display name
	pub fn user_with_display(display: String, fuzzy: Option<bool>) -> Vec<User> {
		Self::user_with_display(display, fuzzy)
	}

	/// fetch user with nick
	pub fn user_with_nick(nick: String, fuzzy: Option<bool>) -> Vec<User> {
		Self::user_with_nick(nick, fuzzy)
	}

	/// fetch user with email
	pub fn user_with_email(email: String) -> Option<User> {
		Self::user_with_email(email)
	}
}

/// schema alias
pub type UserSchema = RootNode<'static, UserRoot, user::UserMutations>;

/// Group root
pub struct GroupRoot;

impl GroupRoot {
	/// fetch by id
	pub fn group_with_id(id: Uuid) -> Option<Group> {
		let group = Database::<Group>::open().unwrap();

		group.read().get(id)
	}

	/// fetch by name
	pub fn group_with_name(name: String, fuzzy: Option<bool>) -> Vec<Group> {
		let groups = Database::<Group>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(name.as_str());

		groups
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.name)
				} else {
					x.name.contains(&name)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch by instance
	pub fn group_with_instance(id: Uuid) -> Vec<Group> {
		let groups = Database::<Group>::open().unwrap();

		groups
			.read()
			.iter()
			.filter(|(_, x)| x.instance == id)
			.map(|(_, x)| x)
			.collect::<Vec<_>>()
	}
}

#[juniper::object(
	context = JwtContext::<reason::Me>
)]
impl GroupRoot {
	/// fetch by id
	pub fn group_with_id(id: Uuid) -> Option<Group> {
		Self::group_with_id(id)
	}

	/// fetch by name
	pub fn group_with_name(name: String, fuzzy: Option<bool>) -> Vec<Group> {
		Self::group_with_name(name, fuzzy)
	}

	/// fetch by instance
	pub fn group_with_instance(id: Uuid) -> Vec<Group> {
		Self::group_with_instance(id)
	}
}

/// schema alias
pub type GroupSchema = RootNode<'static, GroupRoot, group::GroupMutations>;

/// Content root
pub struct ContentRoot;

impl ContentRoot {
	/// fetch object by id
	pub fn content_with_id(id: Uuid) -> Option<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents.read().get(id)
	}

	/// fetch content with instance
	pub fn content_with_instance(id: Uuid) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents.read().iter().filter(|(_, x)| x.instance == id).map(|(_, x)| x).collect()
	}

	/// fetch content of instance with published states
	pub fn content_with_instance_published(id: Uuid, published: bool) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.filter(|(_, x)| x.instance == id && x.published == published)
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch content of instance random
	pub fn content_with_instance_random(id: Uuid, amount: i32) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.take(cmp::max(amount, 0) as usize)
			.filter(|(_, x)| x.instance == id)
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch content of instance by date
	pub fn content_with_instance_date(
		id: Uuid,
		ord: String,
		amount: i32,
	) -> Vec<Content> {
		let mut contempt: Vec<_> = Database::<Content>::open()
			.unwrap()
			.read()
			.iter()
			.filter(|(_, x)| x.instance == id)
			.filter(|(_, x)| x.published)
			.collect();

		match ord.to_lowercase().as_str() {
			"asc" | "ascending" =>
				contempt.sort_by(|a, b| a.1.published_at.cmp(&b.1.published_at)),
			"desc" | "descending" =>
				contempt.sort_by(|a, b| b.1.published_at.cmp(&a.1.published_at)),
			_ => return vec![],
		}

		contempt.into_iter().take(amount as usize).map(|(_, x)| x).collect()
	}

	/// fetch content of instace by date offset
	pub fn content_with_instance_date_offset(
		id: Uuid,
		ord: String,
		start: i32,
		amount: i32,
	) -> Vec<Content> {
		let mut contempt: Vec<_> = Database::<Content>::open()
			.unwrap()
			.read()
			.iter()
			.filter(|(_, x)| x.instance == id)
			.filter(|(_, x)| x.published)
			.collect();

		match ord.to_lowercase().as_str() {
			"asc" | "ascending" =>
				contempt.sort_by(|a, b| a.1.created_at.cmp(&b.1.created_at)),
			"desc" | "descending" =>
				contempt.sort_by(|a, b| b.1.created_at.cmp(&a.1.created_at)),
			_ => return vec![],
		}

		contempt
			.into_iter()
			.skip(start as usize)
			.take(amount as usize)
			.map(|(_, x)| x)
			.collect::<Vec<_>>()
	}

	/// fetch content of instance with tag
	pub fn content_with_instance_tag(id: Uuid, tag: String) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.filter(|(_, x)| x.instance == id && x.tags.contains(&tag))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch content with category
	pub fn content_with_category(category_id: Uuid) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.filter(|(_, x)| x.categories.contains(&category_id))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch content with category children
	pub fn content_with_category_children(category_id: Uuid) -> Vec<Content> {
		use std::collections::HashSet;

		let contents = Database::<Content>::open().unwrap();
		let tree = crate::models::category_tree(category_id).unwrap_or_default();

		contents
			.read()
			.iter()
			.filter(|(_, x)| !x.categories.is_disjoint(&tree))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch content with author
	pub fn content_with_author(id: Uuid) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents.read().iter().filter(|(_, x)| x.author == id).map(|(_, x)| x).collect()
	}

	/// fetch content with slug
	pub fn content_with_slug(instance_id: Uuid, slug: String) -> Option<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.map(|(_, x)| x)
			.find(|x| x.slug == Some(slug.clone()) && x.instance == instance_id)
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl ContentRoot {
	/// fetch object by id
	pub fn content_with_id(id: Uuid) -> Option<Content> {
		ContentRoot::content_with_id(id)
	}

	/// fetch content with instance
	pub fn content_with_instance(id: Uuid) -> Vec<Content> {
		ContentRoot::content_with_instance(id)
	}

	/// fetch content of instance with published states
	pub fn content_with_instance_published(id: Uuid, published: bool) -> Vec<Content> {
		ContentRoot::content_with_instance_published(id, published)
	}

	/// fetch content of instance random
	pub fn content_with_instance_random(id: Uuid, amount: i32) -> Vec<Content> {
		ContentRoot::content_with_instance_random(id, amount)
	}

	/// fetch content of instance by date
	pub fn content_with_instance_date(
		id: Uuid,
		ord: String,
		amount: i32,
	) -> Vec<Content> {
		ContentRoot::content_with_instance_date(id, ord, amount)
	}

	/// fetch content of instace by date offset
	pub fn content_with_instance_date_offset(
		id: Uuid,
		ord: String,
		start: i32,
		amount: i32,
	) -> Vec<Content> {
		ContentRoot::content_with_instance_date_offset(id, ord, start, amount)
	}

	/// fetch content of instance with tag
	pub fn content_with_instance_tag(id: Uuid, tag: String) -> Vec<Content> {
		ContentRoot::content_with_instance_tag(id, tag)
	}

	/// fetch content with category
	pub fn content_with_category(id: Uuid) -> Vec<Content> {
		ContentRoot::content_with_category(id)
	}

	/// fetch content with category children
	pub fn content_with_category_children(category_id: Uuid) -> Vec<Content> {
		ContentRoot::content_with_category_children(category_id)
	}

	/// fetch content with author
	pub fn content_with_author(id: Uuid) -> Vec<Content> {
		ContentRoot::content_with_author(id)
	}

	/// fetch content with slug
	pub fn content_with_slug(instance_id: Uuid, slug: String) -> Option<Content> {
		ContentRoot::content_with_slug(instance_id, slug)
	}
}

/// schema alias
pub type ContentSchema = RootNode<'static, ContentRoot, content::ContentMutations>;

/// Instance root
pub struct InstanceRoot;

impl InstanceRoot {
	/// fetch object by id
	pub fn instance_with_id(id: Uuid) -> Option<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances.read().get(id)
	}

	/// fetch a random amount of instances
	pub fn instance_random(amount: i32) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.take(cmp::max(amount, 0) as usize)
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch a instance by owner
	pub fn instance_with_owner(id: Uuid) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(_, x)| x.owners.contains(&id))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch instance with name
	pub fn instance_with_name(name: String, fuzzy: Option<bool>) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(name.as_str());

		instances
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.name)
				} else {
					x.name.contains(&name)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch instance with tag
	pub fn instance_with_tag(tag: String) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(_, x)| x.tags.contains(&tag))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch instance with domain
	pub fn instance_with_domain(domain: String) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(_, x)| x.domain.contains(&domain))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch all instances
	pub fn instance_all() -> Vec<Instance> {
		Database::<Instance>::open().unwrap().read().iter().map(|(_, x)| x).collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl InstanceRoot {
	/// fetch object by id
	pub fn instance_with_id(id: Uuid) -> Option<Instance> {
		Self::instance_with_id(id)
	}

	/// fetch a random amount of instances
	pub fn instance_random(amount: i32) -> Vec<Instance> {
		Self::instance_random(amount)
	}

	/// fetch a instance by owner
	pub fn instance_with_owner(id: Uuid) -> Vec<Instance> {
		Self::instance_with_owner(id)
	}

	/// fetch instance with name
	pub fn instance_with_name(name: String, fuzzy: Option<bool>) -> Vec<Instance> {
		Self::instance_with_name(name, fuzzy)
	}

	/// fetch instance with tag
	pub fn instance_with_tag(tag: String) -> Vec<Instance> {
		Self::instance_with_tag(tag)
	}

	/// fetch instance with domain
	pub fn instance_with_domain(domain: String) -> Vec<Instance> {
		Self::instance_with_domain(domain)
	}

	/// all instances
	pub fn instance_all() -> Vec<Instance> {
		Self::instance_all()
	}
}

/// schema alias
pub type InstanceSchema = RootNode<'static, InstanceRoot, instance::InstanceMutations>;

/// Category root
pub struct CategoryRoot;

impl CategoryRoot {
	/// fetch object by id
	pub fn category_with_id(id: Uuid) -> Option<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories.read().get(id)
	}

	/// fetch category with parent
	pub fn category_with_parent(id: Uuid) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories
			.read()
			.iter()
			.filter(|(_, x)| x.parent == Some(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch category with instance
	pub fn category_with_instance(id: Uuid) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories.read().iter().filter(|(_, x)| x.owner == id).map(|(_, x)| x).collect()
	}

	/// fetch category with name
	pub fn category_with_name(name: String, fuzzy: Option<bool>) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(name.as_str());

		categories
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.name)
				} else {
					x.name.contains(&name)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch category with slug
	pub fn category_with_slug(slug: String, fuzzy: Option<bool>) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();
		let filter = fuzzy_filter::FuzzyFilter::new(slug.as_str());

		categories
			.read()
			.iter()
			.filter(|(_, x)| {
				if fuzzy == Some(true) {
					filter.matches(&x.slug)
				} else {
					x.slug.contains(&slug)
				}
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// fetch category inactive
	pub fn category_inactive(id: Uuid, is_active: bool) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories
			.read()
			.iter()
			.filter(|(_, x)| x.owner == id && x.active == is_active)
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl CategoryRoot {
	/// fetch object by id
	pub fn category_with_id(id: Uuid) -> Option<Category> {
		Self::category_with_id(id)
	}

	/// fetch category with parent
	pub fn category_with_parent(id: Uuid) -> Vec<Category> {
		Self::category_with_parent(id)
	}

	/// fetch category with instance
	pub fn category_with_instance(id: Uuid) -> Vec<Category> {
		Self::category_with_instance(id)
	}

	/// fetch category with name
	pub fn category_with_name(name: String, fuzzy: Option<bool>) -> Vec<Category> {
		Self::category_with_name(name, fuzzy)
	}

	/// fetch category with slug
	pub fn category_with_slug(slug: String, fuzzy: Option<bool>) -> Vec<Category> {
		Self::category_with_slug(slug, fuzzy)
	}

	/// fetch category inactive
	pub fn category_inactive(id: Uuid, is_active: bool) -> Vec<Category> {
		Self::category_inactive(id, is_active)
	}
}

/// schema alias
pub type CategorySchema = RootNode<'static, CategoryRoot, category::CategoryMutations>;
