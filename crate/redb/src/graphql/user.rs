//! user mutations

/// mutation struct
pub struct UserMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl UserMutations {}
