//! ad mutations

/// mutation struct
pub struct AdMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl AdMutations {}
