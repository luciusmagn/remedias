//! instancce mutations

/// mutation struct
pub struct InstanceMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl InstanceMutations {}
