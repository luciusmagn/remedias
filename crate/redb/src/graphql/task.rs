//! task mutations

/// mutation struct
pub struct TaskMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl TaskMutations {}
