//! group mutations

/// mutation struct
pub struct GroupMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl GroupMutations {}
