//! role mutations

/// mutation struct
pub struct RoleMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl RoleMutations {}
