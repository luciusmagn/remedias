//! content mutations

/// mutation struct
pub struct ContentMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl ContentMutations {}
