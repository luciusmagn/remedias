//! category mutations

/// mutation struct
pub struct CategoryMutations;

use crate::auth::{JwtContext, reason};

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl CategoryMutations {}
