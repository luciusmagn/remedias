//! contains helpful macros

/// Vygeneruje implementaci traity FromDataSimple pro daný typ
#[macro_export]
macro_rules! impl_from_data {
	{$($tp:ty)*} => {$(
		impl FromDataSimple for $tp {
			type Error = String;

			// TODO add limits
			fn from_data(_: &Request, data: Data) -> data::Outcome<Self, Self::Error> {
				// Read the data into a String.
				let mut string = String::new();
				if let Err(e) = data.open().read_to_string(&mut string) {
					return Failure((Status::InternalServerError, e.to_string()));
				}

				match serde_json::from_str(&string) {
					Ok(s) => Success(s),
					Err(_) => Failure((Status::UnprocessableEntity, "invalid JSON".into()))
				}
			}
		}
	)*}
}

/// Vygeneruje FromDataSimple, AsRef a Deref implmenetaci pro daný typ, 1. typ 2. asref typ 3. deref typ
#[macro_export]
macro_rules! impl_trifecta {
	{$($tp:ty => $asref:ty, $deref:ty)*} => {$(
		impl AsRef<$asref> for $tp {
			fn as_ref(&self) -> &$asref {
				&self.0
			}
		}

		impl Deref for $tp {
			type Target = $deref;

			fn deref(&self) -> &Self::Target {
				&self.0
			}
		}

		impl_from_data! { $tp }
	)*}
}

/// Vygeneruje FromDataSimple, AsRef a Deref implmenetaci pro daný typ, 1. typ 2. asref typ 3. deref typ
#[macro_export]
macro_rules! impl_perm {
	{$($tp:ident $(=> $global:literal)*)*} => {$(

	impl Perm for $tp {
		$(fn is_global() -> bool { $global })*
		fn to_reason() -> super::Reason { super::Reason::$tp }
	}

	impl juniper::Context for $tp {}
	)*}
}
