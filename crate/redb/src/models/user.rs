use uuid::Uuid;
use serde::{Serialize, Deserialize};

use std::process::Command;

/// Model uživatele
#[allow(warnings)]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct User {
	/// Identifikátor uživatele
	pub id:        Uuid,
	/// Přezdívka uživatele, lze využít k autentifikaci
	pub nick:      String,
	/// Čitelné jméno uživatele
	pub display:   String,
	/// Email uživatele
	pub email:     String,
	/// Hash jeho hesla
	pub hash:      String,
	/// Potvrzenost účtu
	pub confirmed: bool,
	/// Stránky, ke kterým se uživatel přihlásil
	pub sites:     Vec<Uuid>,
	/// Modifikátory přístupu
	pub perms:     Vec<String>,
	/// Lokace uživatele
	pub location:  Option<String>,
	/// Datum narození
	pub birthdate: String,
	/// Veřejný klíč
	pub pub_key:   String,
	/// Soukromý klíč
	pub priv_key:  String,
	/// role
	pub roles:     Vec<Uuid>,
	///grupa
	pub groups:    Option<Vec<Uuid>>,
}

/// Přijatý uživatel (**R**eceived User)
/// Tuto strukturu přijímá endpoint pro registraci
/// uživatelů.
///
/// Narozdíl od ostatních analogických typů má vynecháno
/// více složek, protože nechceme, aby byl front-end schopen
/// ovlivnit některá důležitá data, jako třeba validní
/// tokeny, jestli je účet již potvrzený, nebo počáteční
/// modifikátory přístupu
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RUser {
	/// přezdívka
	pub nick:      String,
	/// Čitelné jméno
	pub display:   String,
	/// Email uživatele
	pub email:     String,
	/// Hash hesla
	pub pass:      String,
	/// bday
	pub birthdate: String,
	/// Lokace uživatele
	pub location:  Option<String>,
	/// role
	pub roles:     Option<Vec<Uuid>>,
	/// grupa
	pub groups:    Option<Vec<Uuid>>,
}

use crate::Table;
impl Table for User {
	type Key = Uuid;
	type Value = User;

	fn name() -> &'static str {
		"user"
	}
}

use crate::NewEntry;
impl NewEntry for User {
	type Input = RUser;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RUser) -> (Self::Key, User) {
		let id = Uuid::new_v4();
		let hash = super::super::make_hash(&src.pass);
		let keys = String::from_utf8(
			Command::new("nim/keymaster").output().expect("sad story").stdout,
		)
		.unwrap()
		.split('|')
		.map(|x| x.to_string())
		.collect::<Vec<String>>();
		let (priv_key, pub_key) = (keys[0].to_string(), keys[1].to_string());

		(id, User {
			id,
			display: src.display,
			email: src.email,
			hash,
			confirmed: false,
			sites: vec![],
			perms: vec![],
			location: src.location,
			birthdate: src.birthdate,
			nick: src.nick,
			pub_key,
			priv_key,
			roles: src.roles.unwrap_or_default(),
			groups: src.groups,
		})
	}
}

use crate::Database;
use super::{task::Task, role::Role, content::Content, instance::Instance, group::Group};
use crate::auth::{reason, Reason, JwtContext};

use std::str::FromStr;

impl User {
	/// Identifikátor uživatele
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// Přezdívka uživatele, lze využít k autentifikaci
	pub fn nick(&self) -> &str {
		self.nick.as_str()
	}

	/// Čitelné jméno uživatele
	pub fn display(&self) -> &str {
		self.display.as_str()
	}

	/// Email uživatele
	pub fn email(&self) -> &str {
		self.email.as_str()
	}

	/// Potvrzenost účtu
	pub fn confirmed(&self) -> bool {
		self.confirmed
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms.clone()
	}

	/// Lokace uživatele
	pub fn location(&self) -> Option<String> {
		self.location.clone()
	}

	/// Datum narození
	pub fn birthdate(&self) -> String {
		self.birthdate.clone()
	}

	/*
		** non-trivial
		*/

	/// Stránky, ke kterým se uživatel přihlásil
	pub fn sites(&self) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(id, _)| self.sites.contains(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// owned instances
	pub fn owned_sites(&self) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(_, x)| x.owners.contains(&self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// admin instances
	pub fn admin_sites(&self) -> Vec<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances
			.read()
			.iter()
			.filter(|(id, _)| self.perms.contains(&id.to_hyphenated().to_string()))
			.filter(|(id, _)| {
				self.perms
					.iter()
					.filter(|p| p.contains(&id.to_hyphenated().to_string()))
					.map(|p| Reason::from_str(p).unwrap())
					.any(|p| p.is_local_admin())
			})
			.map(|(_, x)| x)
			.collect()
	}

	/// role
	pub fn roles(&self) -> Vec<Role> {
		let roles = Database::<Role>::open().unwrap();

		roles
			.read()
			.iter()
			.filter(|(id, _)| self.roles.contains(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// group
	pub fn groups(&self) -> Option<Vec<Group>> {
		let groups_db = Database::<Group>::open().unwrap();

		Some(
			groups_db
				.read()
				.iter()
				.filter(|(id, _)| self.groups.clone().unwrap_or_default().contains(&id))
				.map(|(_, x)| x)
				.collect::<Vec<_>>(),
		)
	}

	/// tasks, where user is assignee
	pub fn tasks_assignee(&self) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.assignee_id == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// tasks, where user is reviewer
	pub fn tasks_reviewer(&self) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.reviewer_id == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// tasks, where user is creator
	pub fn tasks_creator(&self) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.creator_id == self.id)
			.map(|(_, x)| x)
			.collect()
	}

	/// content by user
	pub fn articles(&self) -> Vec<Content> {
		let content = Database::<Content>::open().unwrap();

		content
			.read()
			.iter()
			.filter(|(_, x)| x.author == self.id)
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl User {
	/// Identifikátor uživatele
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// Přezdívka uživatele, lze využít k autentifikaci
	pub fn nick(&self) -> &str {
		self.nick()
	}

	/// Čitelné jméno uživatele
	pub fn display(&self) -> &str {
		self.display()
	}

	/// Email uživatele
	pub fn email(&self) -> &str {
		self.email()
	}

	/// Potvrzenost účtu
	pub fn confirmed(&self) -> bool {
		self.confirmed()
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms()
	}

	/// Lokace uživatele
	pub fn location(&self) -> Option<String> {
		self.location()
	}

	/// Datum narození
	pub fn birthdate(&self) -> String {
		self.birthdate()
	}

	/*
		** non-trivial
		*/

	/// Stránky, ke kterým se uživatel přihlásil
	pub fn sites(&self) -> Vec<Instance> {
		self.sites()
	}

	/// owned instances
	pub fn owned_sites(&self) -> Vec<Instance> {
		self.owned_sites()
	}

	/// admin instances
	pub fn admin_sites(&self) -> Vec<Instance> {
		self.admin_sites()
	}

	/// role
	pub fn roles(&self) -> Vec<Role> {
		self.roles()
	}

	/// group
	pub fn groups(&self) -> Option<Vec<Group>> {
		self.groups()
	}

	/// tasks, where user is assignee
	pub fn tasks_assignee(&self) -> Vec<Task> {
		self.tasks_assignee()
	}

	/// tasks, where user is reviewer
	pub fn tasks_reviewer(&self) -> Vec<Task> {
		self.tasks_reviewer()
	}

	/// tasks, where user is creator
	pub fn tasks_creator(&self) -> Vec<Task> {
		self.tasks_creator()
	}

	/// content by user
	pub fn articles(&self) -> Vec<Content> {
		self.articles()
	}
}
