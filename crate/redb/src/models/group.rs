use uuid::Uuid;
use serde::{Serialize, Deserialize};

/// skupina
#[allow(warnings)]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Group {
	/// id
	pub id:          Uuid,
	/// zbývající kliky
	pub name:        String,
	/// html reklamy
	pub description: String,
	/// tags
	pub role:        Option<Uuid>,
	/// vlastník reklamy
	pub instance:    Uuid,
}

/// Přijatá skupina (**R**)Group
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RGroup {
	/// slug, pod kterým bude UUID dostupné
	pub name:        String,
	/// cílové UUID
	pub description: String,
	/// tags
	pub role:        Option<Uuid>,
}

/*
impl RGroup {
	/// Metoda pro konverzi RGroup -> IGroup
	pub fn with_id(&self, id: Uuid) -> IGroup {
		IGroup { instance: id, clicks: self.clicks, html: self.html.clone(), tags: self.tags.clone() }
	}
}
*/

use crate::Table;
/// category table
impl Table for Group {
	type Key = Uuid;
	type Value = Group;

	fn name() -> &'static str {
		"group"
	}
}

use crate::NewEntry;
impl NewEntry for Group {
	type Input = RGroup;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RGroup) -> (Uuid, Group) {
		let id = Uuid::new_v4();

		(id, Group {
			id,
			name: src.name,
			description: src.description,
			role: src.role,
			instance: Uuid::new_v4(),
		})
	}
}

use crate::auth::{reason, JwtContext};
use crate::Database;
use super::{user::User, content::Content, instance::Instance, role::Role};

impl Group {
	/// id
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// zbývající kliky
	pub fn name(&self) -> &str {
		self.name.as_str()
	}

	/// html reklamy
	pub fn description(&self) -> &str {
		self.description.as_str()
	}

	/*
		** Non-trivial
		*/

	/// tags
	pub fn role(&self) -> Option<Role> {
		let roles = Database::<Role>::open().unwrap();

		roles.read().get(self.role?)
	}

	/// vlastník reklamy
	pub fn instance(&self) -> Option<Instance> {
		let instances = Database::<Instance>::open().unwrap();

		instances.read().get(self.instance)
	}

	/// users
	pub fn users(&self) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter_map(|(id, x)| {
				if x.groups.clone().unwrap_or_default().contains(&id) {
					Some(x)
				} else {
					None
				}
			})
			.collect()
	}

	/// content
	pub fn content(&self) -> Vec<Content> {
		let contents = Database::<Content>::open().unwrap();

		contents
			.read()
			.iter()
			.filter_map(|(_, x)| if x.group == Some(self.id) { Some(x) } else { None })
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Group {
	/// id
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// zbývající kliky
	pub fn name(&self) -> &str {
		self.name()
	}

	/// html reklamy
	pub fn description(&self) -> &str {
		self.description()
	}

	/*
		** Non-trivial
		*/

	/// tags
	pub fn role(&self) -> Option<Role> {
		self.role()
	}

	/// vlastník reklamy
	pub fn instance(&self) -> Option<Instance> {
		self.instance()
	}

	/// users
	pub fn users(&self) -> Vec<User> {
		self.users()
	}

	/// content
	pub fn content(&self) -> Vec<Content> {
		self.content()
	}
}
