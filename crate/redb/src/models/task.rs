use uuid::Uuid;
use serde::{Serialize, Deserialize};

/// Task
#[allow(warnings)]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Task {
	/// Id úkolu
	pub id:              Uuid,
	/// mateřská instance
	pub instance:        Uuid,
	/// název úkolu
	pub title:           String,
	/// obsah/popis
	pub content:         String,
	/// ??? TODO
	pub after_completed: String,
	/// Id pracovníka
	pub assignee_id:     Option<Uuid>,
	/// Id reviewera
	pub reviewer_id:     Option<Uuid>,
	/// Id tvůrce
	pub creator_id:      Uuid,
	/// Kategorie
	pub category:        Option<Uuid>,
	/// Priorita
	pub priority:        String,
	/// Deadline
	pub due_at:          Option<i32>,
	/// status
	pub state:           String,
	/// group
	pub group:           Option<Uuid>,
}

/// Přijatý Task
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RTask {
	/// název úkolu
	pub title:           String,
	/// obsah úkolu
	pub content:         String,
	/// ??? TODO
	pub after_completed: String,
	/// id pracovníka
	pub assignee_id:     Option<Uuid>,
	/// id reviewera
	pub reviewer_id:     Option<Uuid>,
	/// kategorie
	pub category:        Option<Uuid>,
	/// priorita
	pub priority:        String,
	/// čas
	pub due_at:          Option<i32>,
	/// group
	pub group:           Option<Uuid>,
}

use crate::Table;
impl Table for Task {
	type Key = Uuid;
	type Value = Task;

	fn name() -> &'static str {
		"task"
	}
}

use crate::NewEntry;
impl NewEntry for Task {
	type Input = RTask;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RTask) -> (Uuid, Task) {
		let id = Uuid::new_v4();

		(id, Task {
			id,
			title: src.title,
			content: src.content,
			after_completed: src.after_completed,
			assignee_id: src.assignee_id,
			reviewer_id: src.reviewer_id,
			creator_id: Uuid::new_v4(),
			category: src.category,
			priority: src.priority,
			due_at: src.due_at,
			instance: Uuid::new_v4(),
			state: "new".into(), // TODO proper fucking type
			group: src.group,
		})
	}
}

use crate::auth::{reason, JwtContext};
use crate::Database;
use super::{
	user::User, content::Content, category::Category, instance::Instance, group::Group,
};

impl Task {
	/// Id úkolu
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// název úkolu
	pub fn title(&self) -> &str {
		self.title.as_str()
	}

	/// obsah/popis
	pub fn content(&self) -> &str {
		self.content.as_str()
	}

	/// ??? TODO
	pub fn after_completed(&self) -> &str {
		self.after_completed.as_str()
	}

	/// Priorita
	pub fn priority(&self) -> &str {
		self.priority.as_str()
	}

	/// Deadline
	pub fn due_at(&self) -> Option<i32> {
		self.due_at
	}

	/// status
	pub fn state(&self) -> &str {
		self.state.as_str()
	}

	/*
		** non-trivial
		*/

	/// mateřská instance
	pub fn instance(&self) -> Instance {
		let instances = Database::<Instance>::open().unwrap();

		instances.read().get(self.instance).unwrap()
	}

	/// pracovník
	pub fn assignee(&self) -> Option<User> {
		let users = Database::<User>::open().unwrap();

		users.read().get(self.assignee_id?)
	}

	/// reviewer
	pub fn reviewer(&self) -> Option<User> {
		let users = Database::<User>::open().unwrap();

		users.read().get(self.reviewer_id?)
	}

	/// tvůrce
	pub fn creator(&self) -> User {
		let users = Database::<User>::open().unwrap();

		users.read().get(self.creator_id).unwrap()
	}

	/// group
	pub fn group(&self) -> Option<Group> {
		let groups = Database::<Group>::open().unwrap();

		groups.read().get(self.group?)
	}

	/// Kategorie
	pub fn category(&self) -> Option<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories.read().get(self.category?)
	}

	/// content
	pub fn content_list(&self) -> Vec<Content> {
		let content = Database::<Content>::open().unwrap();

		content
			.read()
			.iter()
			.filter(|(_, x)| x.task_id == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Task {
	/// Id úkolu
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// název úkolu
	pub fn title(&self) -> &str {
		self.title()
	}

	/// obsah/popis
	pub fn content(&self) -> &str {
		self.content()
	}

	/// ??? TODO
	pub fn after_completed(&self) -> &str {
		self.after_completed()
	}

	/// Priorita
	pub fn priority(&self) -> &str {
		self.priority()
	}

	/// Deadline
	pub fn due_at(&self) -> Option<i32> {
		self.due_at()
	}

	/// status
	pub fn state(&self) -> &str {
		self.state()
	}

	/*
		** non-trivial
		*/

	/// mateřská instance
	pub fn instance(&self) -> Instance {
		self.instance()
	}

	/// pracovník
	pub fn assignee(&self) -> Option<User> {
		self.assignee()
	}

	/// reviewer
	pub fn reviewer(&self) -> Option<User> {
		self.reviewer()
	}

	/// tvůrce
	pub fn creator(&self) -> User {
		self.creator()
	}

	/// group
	pub fn group(&self) -> Option<Group> {
		self.group()
	}

	/// Kategorie
	pub fn category(&self) -> Option<Category> {
		self.category()
	}

	/// content
	pub fn content_list(&self) -> Vec<Content> {
		self.content_list()
	}
}
