//! Modely jednotlivých tabulek.
use rocket::data::{self, FromDataSimple};
use rocket::{Request, Data, Outcome::*};
use rocket::http::Status;

use std::io::Read;

mod category;
pub use self::category::*;

mod content;
pub use self::content::*;

mod instance;
pub use self::instance::*;

mod group;
pub use self::group::*;

mod user;
pub use self::user::*;

mod task;
pub use self::task::*;

mod role;
pub use self::role::*;

mod ad;
pub use self::ad::*;

impl_from_data! { RCategory RContent RInstance RUser RAd RTask Task RRole Role RGroup Group }
