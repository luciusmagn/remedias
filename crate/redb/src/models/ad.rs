use uuid::Uuid;
use serde::{Serialize, Deserialize};

/// Reklama
#[allow(warnings)]
#[derive(Clone, Debug, Serialize, Deserialize, juniper::GraphQLObject)]
pub struct Ad {
	/// id
	pub id:     Uuid,
	/// zbývající kliky
	pub clicks: i32,
	/// html reklamy
	pub html:   String,
	/// tags
	pub tags:   Vec<String>,
	/// vlastník reklamy
	pub owner:  Uuid,
}

/// Přijatá reklama (**R**)Ad
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RAd {
	/// slug, pod kterým bude UUID dostupné
	pub clicks: i32,
	/// cílové UUID
	pub html:   String,
	/// tags
	pub tags:   Vec<String>,
}

/*
impl RAd {
	/// Metoda pro konverzi RAd -> IAd
	pub fn with_id(&self, id: Uuid) -> IAd {
		IAd { owner: id, clicks: self.clicks, html: self.html.clone(), tags: self.tags.clone() }
	}
}
*/

use crate::Table;
/// category table
impl Table for Ad {
	type Key = Uuid;
	type Value = Ad;

	fn name() -> &'static str {
		"ad"
	}
}

use crate::NewEntry;
impl NewEntry for Ad {
	type Input = RAd;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RAd) -> (Uuid, Ad) {
		let id = Uuid::new_v4();

		(id, Ad {
			id,
			clicks: src.clicks,
			html: src.html,
			tags: src.tags,
			owner: Uuid::new_v4(),
		})
	}
}
