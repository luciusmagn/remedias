use uuid::Uuid;
use serde::{Serialize, Deserialize};

/// Model instance, tj. 'miniserveru'
/// K instancím se řadí veškerý obsah
/// v projektu Meedias
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Instance {
	/// Identifikátor instance
	pub id:     Uuid,
	/// Čitelný název stránky
	pub name:   String,
	/// Doména stránky
	pub domain: String,
	/// UUID všech vlastníků dané instance
	pub owners: Vec<Uuid>,
	/// Globální tagy přiřazené dané instanci
	pub tags:   Vec<String>,
}

/// Struktura pro přijímání instance
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RInstance {
	/// Čitelný název stránky
	pub name:   String,
	/// Doména
	pub domain: String,
	/// Vlastnící (tvůrce je zde automaticky)
	pub owners: Option<Vec<Uuid>>,
	/// Globální tagy
	pub tags:   Vec<String>,
}

use crate::Table;
impl Table for Instance {
	type Key = Uuid;
	type Value = Instance;

	fn name() -> &'static str {
		"instance"
	}
}

use crate::NewEntry;
impl NewEntry for Instance {
	type Input = RInstance;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RInstance) -> (Uuid, Instance) {
		let id = Uuid::new_v4();

		(id, Instance {
			id,
			name: src.name,
			domain: src.domain,
			owners: src.owners.unwrap_or_default(),
			tags: src.tags,
		})
	}
}

use crate::Database;
use super::{user::User, task::Task, content::Content, category::Category};
use crate::auth::{reason, Reason, JwtContext};

use std::str::FromStr;

impl Instance {
	/// returns id of the instance
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// returns the name of the instance
	pub fn name(&self) -> &str {
		self.name.as_str()
	}

	/// returns the domain of the instance
	pub fn domain(&self) -> &str {
		self.domain.as_str()
	}

	/// returns owners of the instance
	pub fn owners(&self) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter(|(id, _)| self.owners.contains(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// returns tags
	pub fn tags(&self) -> Vec<String> {
		self.tags.clone()
	}

	/*
		** non-trivial
		*/

	/// returns all categories of a given instance
	pub fn categories(&self) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories
			.read()
			.iter()
			.filter(|(_, x)| x.owner == self.id)
			.map(|(_, x)| x)
			.collect()
	}

	/// returns all known users to ever log in to the instance
	pub fn users(&self) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter(|(_, x)| x.sites.contains(&self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// returns every user with at least one admin permission for this instance
	pub fn admins(&self) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter(|(_, x)| {
				x.perms
					.iter()
					.filter(|x| x.contains(&self.id.to_hyphenated().to_string()))
					.filter_map(|x| Reason::from_str(x).ok())
					.any(|x| x.is_local_admin())
			})
			.map(|(_, x)| dbg!(x))
			.chain(dbg!(self.owners()).into_iter())
			.collect()
	}

	/// returns all tasks for a given instance
	pub fn tasks(&self) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.instance == self.id)
			.map(|(_, x)| x)
			.collect()
	}

	/// returns all articles for a given instance
	pub fn articles(&self) -> Vec<Content> {
		let content = Database::<Content>::open().unwrap();

		content
			.read()
			.iter()
			.filter(|(_, x)| x.instance == self.id)
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Instance {
	/// returns id of the instance
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// returns the name of the instance
	pub fn name(&self) -> &str {
		self.name()
	}

	/// returns the domain of the instance
	pub fn domain(&self) -> &str {
		self.domain()
	}

	/// returns owners of the instance
	pub fn owners(&self) -> Vec<User> {
		self.owners()
	}

	/// returns tags
	pub fn tags(&self) -> Vec<String> {
		self.tags()
	}

	/*
		** non-trivial
		*/

	/// returns all categories of a given instance
	pub fn categories(&self) -> Vec<Category> {
		self.categories()
	}

	/// returns all known users to ever log in to the instance
	pub fn users(&self) -> Vec<User> {
		self.users()
	}

	/// returns every user with at least one admin permission for this instance
	pub fn admins(&self) -> Vec<User> {
		self.admins()
	}

	/// returns all tasks for a given instance
	pub fn tasks(&self) -> Vec<Task> {
		self.tasks()
	}

	/// returns all articles for a given instance
	pub fn articles(&self) -> Vec<Content> {
		self.articles()
	}
}
