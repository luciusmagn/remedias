use uuid::Uuid;
use crate::Database;
use serde::{Serialize, Deserialize};

use std::collections::HashSet;

/// Model role
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Role {
	/// Identifikátor role
	pub id:       Uuid,
	/// Vlastnící instance
	pub instance: Uuid,
	/// Jméno role
	pub name:     String,
	/// UUID všech vlastníků dané instance
	pub perms:    Vec<String>,
	/// Globální tagy přiřazené dané instanci
	pub parent:   Option<Uuid>,
}

/// Struktura pro přijímání instance
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RRole {
	/// Jméno role
	pub name:   String,
	/// UUID všech vlastníků dané instance
	pub perms:  Vec<String>,
	/// Globální tagy přiřazené dané instanci
	pub parent: Option<Uuid>,
}

use crate::Table;
impl Table for Role {
	type Key = Uuid;
	type Value = Role;

	fn name() -> &'static str {
		"role"
	}
}

use crate::NewEntry;
impl NewEntry for Role {
	type Input = RRole;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RRole) -> (Uuid, Role) {
		let id = Uuid::new_v4();

		(id, Role {
			id,
			instance: Uuid::new_v4(),
			perms: src.perms,
			parent: src.parent,
			name: src.name,
		})
	}
}

/// collects all sub-categories
#[allow(clippy::redundant_clone)]
pub fn role_tree(id: Uuid) -> Option<HashSet<Uuid>> {
	let mut ids = Database::<Role>::open()?
		.read()
		.iter()
		.filter_map(|(i, x)| if x.parent == Some(id) { Some(i) } else { None })
		.collect::<HashSet<_>>();

	ids.clone().iter().for_each(|i| ids.extend(role_tree(*i).unwrap_or_default()));

	Some(ids)
}

use super::{user::User, instance::Instance};
use crate::auth::{reason, JwtContext};

impl Role {
	/// Identifikátor role
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// Jméno role
	pub fn name(&self) -> &str {
		self.name.as_str()
	}

	/// perms
	pub fn perms(&self) -> Vec<String> {
		self.perms.clone()
	}

	/*
		** non-trivial
		*/

	/// Vlastnící instance
	pub fn instance(&self) -> Instance {
		let instances = Database::<Instance>::open().unwrap();

		instances.read().get(self.instance).unwrap()
	}

	/// parent of the role
	pub fn parent(&self) -> Option<Role> {
		let roles = Database::<Role>::open().unwrap();

		roles.read().get(self.parent?)
	}

	/// direct children
	pub fn direct_children(&self) -> Vec<Role> {
		let roles = Database::<Role>::open().unwrap();

		roles
			.read()
			.iter()
			.filter(|(_, x)| x.parent == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// all children
	pub fn all_children(&self) -> Vec<Role> {
		let roles = Database::<Role>::open().unwrap();

		role_tree(self.id).unwrap().iter().filter_map(|x| roles.read().get(x)).collect()
	}

	/// users with role
	pub fn users(&self) -> Vec<User> {
		let users = Database::<User>::open().unwrap();

		users
			.read()
			.iter()
			.filter(|(_, x)| x.roles.contains(&self.id))
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Role {
	/// Identifikátor role
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// Jméno role
	pub fn name(&self) -> &str {
		self.name()
	}

	/// perms
	pub fn perms(&self) -> Vec<String> {
		self.perms()
	}

	/*
		** non-trivial
		*/

	/// Vlastnící instance
	pub fn instance(&self) -> Instance {
		self.instance()
	}

	/// parent of the role
	pub fn parent(&self) -> Option<Role> {
		self.parent()
	}

	/// direct children
	pub fn direct_children(&self) -> Vec<Role> {
		self.direct_children()
	}

	/// all children
	pub fn all_children(&self) -> Vec<Role> {
		self.all_children()
	}

	/// users with role
	pub fn users(&self) -> Vec<User> {
		self.users()
	}
}
