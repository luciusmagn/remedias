use uuid::Uuid;
use chrono::Utc;
use serde::{Serialize, Deserialize};

use std::fmt;
use std::collections::HashSet;

/// Model obsahu, tj. článku
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Content {
	/// Identifikátor obsahu
	pub id:           Uuid,
	/// Vlastnící instance
	pub instance:     Uuid,
	/// UUID autora
	pub author:       Uuid,
	/// Modifikátory přístupu
	pub perms:        Vec<String>,
	/// Čitelný název / Nadpis článku
	pub name:         String,
	/// Obsah
	pub contents:     String,
	/// Tagy přiřazené článku
	pub tags:         Vec<String>,
	/// UUID kategorií, do kterých je zařazen
	pub categories:   HashSet<Uuid>,
	/// Lokace
	pub location:     Option<String>,
	/// Řetězec změn autorů
	pub changes:      Vec<String>,
	/// Počet zobrazení
	pub views:        i32,
	/// Popisek
	pub description:  Option<String>,
	/// Url přidruženého obrázku
	pub image_url:    Option<String>,
	/// Published
	pub published:    bool,
	/// Id úkolu
	pub task_id:      Option<Uuid>,
	/// group
	pub group:        Option<Uuid>,
	/// slug
	pub slug:         Option<String>,
	/// Datum plánované publikace
	pub publish_at:   Option<String>,
	/// Datum faktické publikace
	pub published_at: Option<String>,
	/// Datum vytvoření
	pub created_at:   String,
	/// Datum aktualizace
	pub updated_at:   String,
}

/// Přijatý obsah (**R**eceived `Content`)
/// Tuto strukturu přijímá endpoint pro vkládání nového obsahu
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RContent {
	/// Autor obsahu
	pub author:      Uuid,
	/// Modifikátory přístupu
	pub perms:       Vec<String>,
	/// Čitelný název
	pub name:        String,
	/// Samotný obsah
	pub contents:    String,
	/// Tagy přiřazené článku
	pub tags:        Vec<String>,
	/// UUID kategorií, do kterých je zařazen
	pub categories:  HashSet<Uuid>,
	/// Lokace
	pub location:    Option<String>,
	/// Popisek
	pub description: Option<String>,
	/// Url přidruženého obrázku
	pub image_url:   Option<String>,
	/// Id úkolu
	pub task_id:     Option<Uuid>,
	/// group
	pub group:       Option<Uuid>,
	/// slug
	pub slug:        Option<String>,
	/// Datum plánované publikace
	pub publish_at:  Option<String>,
}

impl fmt::Display for RContent {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		writeln!(f, "Author: {}", self.author.to_hyphenated())?;
		writeln!(f, "Name: {}", &self.name)?;
		writeln!(
			f,
			"Location: {}",
			self.location.clone().unwrap_or_else(|| "<none>".to_string())
		)?;
		writeln!(f, "{}", &self.contents)
	}
}

use crate::Table;
impl Table for Content {
	type Key = Uuid;
	type Value = Content;

	fn name() -> &'static str {
		"content"
	}
}

use crate::NewEntry;
impl NewEntry for Content {
	type Input = RContent;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RContent) -> (Uuid, Content) {
		let id = Uuid::new_v4();

		(id, Content {
			id,
			instance: Uuid::new_v4(),
			author: src.author,
			perms: vec![],
			name: src.name,
			contents: src.contents,
			tags: src.tags,
			categories: src.categories,
			location: src.location,
			changes: vec![],
			views: 0,
			description: src.description,
			image_url: src.image_url,
			published: false,
			task_id: src.task_id,
			group: src.group,
			slug: src.slug,
			publish_at: src.publish_at,
			published_at: None,
			created_at: Utc::now().to_rfc3339(),
			updated_at: Utc::now().to_rfc3339(),
		})
	}
}

use crate::auth::{reason, JwtContext};
use crate::Database;
use super::{user::User, task::Task, category::Category, group::Group};

impl Content {
	/// Identifikátor obsahu
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// Vlastnící instance
	pub fn instance(&self) -> Uuid {
		self.instance
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms.clone()
	}

	/// Čitelný název / Nadpis článku
	pub fn name(&self) -> &str {
		self.name.as_str()
	}

	/// Obsah
	pub fn contents(&self) -> &str {
		self.contents.as_str()
	}

	/// Tagy přiřazené článku
	pub fn tags(&self) -> Vec<String> {
		self.tags.clone()
	}

	/// Lokace
	pub fn location(&self) -> Option<String> {
		self.location.clone()
	}

	/// Lokace
	pub fn slug(&self) -> Option<String> {
		self.slug.clone()
	}

	/// Počet zobrazení
	pub fn views(&self) -> i32 {
		self.views
	}

	/// Popisek
	pub fn description(&self) -> Option<String> {
		self.description.clone()
	}

	/// Url přidruženého obrázku
	pub fn image_url(&self) -> Option<String> {
		self.image_url.clone()
	}

	/// Published
	pub fn published(&self) -> bool {
		self.published
	}

	/// datum plánované publikace
	pub fn publish_at(&self) -> Option<String> {
		self.publish_at.clone()
	}

	/// datum plánované publikace
	pub fn published_at(&self) -> Option<String> {
		self.publish_at.clone()
	}

	/// Datum vytvoření
	pub fn created_at(&self) -> &str {
		self.created_at.as_str() // TODO proper type
	}

	/// Datum vytvoření
	pub fn updated_at(&self) -> &str {
		self.updated_at.as_str() // TODO proper type
	}

	/*
		** non-trivial
		*/

	/// UUID autora
	pub fn author(&self) -> User {
		let users = Database::<User>::open().unwrap();

		users.read().get(&self.author).unwrap() // TODO error handling
	}

	/// group
	pub fn group(&self) -> Option<Group> {
		let groups = Database::<Group>::open().unwrap();

		groups.read().get(&self.group?)
	}

	/// UUID kategorií, do kterých je zařazen
	pub fn categories(&self) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories
			.read()
			.iter()
			.filter(|(id, _)| self.categories.contains(id))
			.map(|(_, x)| x)
			.collect()
	}

	/// Id úkolu
	pub fn task(&self) -> Option<Task> {
		let tasks = Database::<Task>::open().unwrap();
		let id = self.task_id?;

		tasks.read().get(&id)
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Content {
	/// Identifikátor obsahu
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// Vlastnící instance
	pub fn instance(&self) -> Uuid {
		self.instance()
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms()
	}

	/// Čitelný název / Nadpis článku
	pub fn name(&self) -> &str {
		self.name()
	}

	/// Obsah
	pub fn contents(&self) -> &str {
		self.contents()
	}

	/// Tagy přiřazené článku
	pub fn tags(&self) -> Vec<String> {
		self.tags()
	}

	/// Lokace
	pub fn location(&self) -> Option<String> {
		self.location()
	}

	/// Lokace
	pub fn slug(&self) -> Option<String> {
		self.slug()
	}

	/// Počet zobrazení
	pub fn views(&self) -> i32 {
		self.views()
	}

	/// Popisek
	pub fn description(&self) -> Option<String> {
		self.description()
	}

	/// Url přidruženého obrázku
	pub fn image_url(&self) -> Option<String> {
		self.image_url()
	}

	/// Published
	pub fn published(&self) -> bool {
		self.published()
	}

	/// datum plánované publikace
	pub fn publish_at(&self) -> Option<String> {
		self.publish_at()
	}

	/// datum plánované publikace
	pub fn published_at(&self) -> Option<String> {
		self.publish_at()
	}

	/// Datum vytvoření
	pub fn created_at(&self) -> &str {
		self.created_at() // TODO proper type
	}

	/// Datum vytvoření
	pub fn updated_at(&self) -> &str {
		self.updated_at() // TODO proper type
	}

	/*
		** non-trivial
		*/

	/// UUID autora
	pub fn author(&self) -> User {
		self.author()
	}

	/// group
	pub fn group(&self) -> Option<Group> {
		self.group()
	}

	/// UUID kategorií, do kterých je zařazen
	pub fn categories(&self) -> Vec<Category> {
		self.categories()
	}

	/// Id úkolu
	pub fn task(&self) -> Option<Task> {
		self.task()
	}
}
