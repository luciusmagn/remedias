use uuid::Uuid;
use crate::Database;
use serde::{Serialize, Deserialize};

use std::collections::HashSet;

/// Model kategorie
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Category {
	/// Identifikátor kategorie
	pub id:     Uuid,
	/// Domovská instance
	pub owner:  Uuid,
	/// Slug kategorie
	pub slug:   String,
	/// Čitelný název
	pub name:   String,
	/// Nadkategorie
	pub parent: Option<Uuid>,
	/// Modifikátory přístupu
	pub perms:  Vec<String>,
	/// Stav
	pub active: bool,
}

/// Přijatá kategorie (**R**eceived `Category`)
/// Tuto strukturu přijímá endpoint pro vkládání nových kategorií
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RCategory {
	/// Slug kategorie
	pub slug:   String,
	/// Čitelný název kategorie
	pub name:   String,
	/// Nadkategorie
	pub parent: Option<Uuid>,
	/// Modifikátory přístupu
	pub perms:  Vec<String>,
	/// stav
	pub active: Option<bool>,
}

use crate::Table;
impl Table for Category {
	type Key = Uuid;
	type Value = Category;

	fn name() -> &'static str {
		"category"
	}
}

use crate::NewEntry;
impl NewEntry for Category {
	type Input = RCategory;
	type Key = <Self as Table>::Key;
	type Table = Self;

	fn create(src: RCategory) -> (Uuid, Category) {
		let id = Uuid::new_v4();

		(id, Category {
			id,
			owner: Uuid::new_v4(),
			slug: src.slug,
			name: src.name,
			parent: src.parent,
			perms: src.perms,
			active: src.active.unwrap_or(true),
		})
	}
}

/// collects all sub-categories
#[allow(clippy::redundant_clone)] // ids.clone() line causes a false alarm in clippy
pub fn category_tree(id: Uuid) -> Option<HashSet<Uuid>> {
	let mut ids = Database::<Category>::open()?
		.read()
		.iter()
		.filter_map(|(i, x)| if x.parent == Some(id) { Some(i) } else { None })
		.collect::<HashSet<_>>();

	ids.clone().iter().for_each(|i| ids.extend(category_tree(*i).unwrap_or_default()));

	Some(ids)
}

use super::{task::Task, content::Content, instance::Instance};
use crate::{auth::JwtContext, auth::reason};

impl Category {
	/// Identifikátor kategorie
	pub fn id(&self) -> Uuid {
		self.id
	}

	/// Slug kategorie
	pub fn slug(&self) -> &str {
		self.slug.as_str()
	}

	/// Čitelný název
	pub fn name(&self) -> &str {
		self.name.as_str()
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms.clone()
	}

	/// Stav
	pub fn active(&self) -> bool {
		self.active
	}

	/*
		** non-trivial
		*/

	/// Nadkategorie
	pub fn parent(&self) -> Option<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories.read().get(self.parent?)
	}

	/// direct children
	pub fn direct_children(&self) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		categories
			.read()
			.iter()
			.filter(|(_, x)| x.parent == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// all children
	pub fn all_children(&self) -> Vec<Category> {
		let categories = Database::<Category>::open().unwrap();

		category_tree(self.id)
			.unwrap()
			.iter()
			.filter_map(|x| categories.read().get(x))
			.collect()
	}

	/// Domovská instance
	pub fn owner(&self) -> Instance {
		let instances = Database::<Instance>::open().unwrap();

		instances.read().get(self.owner).unwrap()
	}

	/// return content
	pub fn content(&self) -> Vec<Content> {
		let content = Database::<Content>::open().unwrap();

		content
			.read()
			.iter()
			.filter(|(_, x)| x.categories.contains(&self.id))
			.map(|(_, x)| x)
			.collect()
	}

	/// return tasks
	pub fn tasks(&self) -> Vec<Task> {
		let tasks = Database::<Task>::open().unwrap();

		tasks
			.read()
			.iter()
			.filter(|(_, x)| x.category == Some(self.id))
			.map(|(_, x)| x)
			.collect()
	}
}

#[juniper::object(
	context = JwtContext<reason::Me>
)]
impl Category {
	/// Identifikátor kategorie
	pub fn id(&self) -> Uuid {
		self.id()
	}

	/// Slug kategorie
	pub fn slug(&self) -> &str {
		self.slug()
	}

	/// Čitelný název
	pub fn name(&self) -> &str {
		self.name()
	}

	/// Modifikátory přístupu
	pub fn perms(&self) -> Vec<String> {
		self.perms()
	}

	/// Stav
	pub fn active(&self) -> bool {
		self.active()
	}

	/*
		** non-trivial
		*/

	/// Nadkategorie
	pub fn parent(&self) -> Option<Category> {
		self.parent()
	}

	/// direct children
	pub fn direct_children(&self) -> Vec<Category> {
		self.direct_children()
	}

	/// all children
	pub fn all_children(&self) -> Vec<Category> {
		self.all_children()
	}

	/// Domovská instance
	pub fn owner(&self) -> Instance {
		self.owner()
	}

	/// return content
	pub fn content(&self) -> Vec<Content> {
		self.content()
	}

	/// return tasks
	pub fn tasks(&self) -> Vec<Task> {
		self.tasks()
	}
}
