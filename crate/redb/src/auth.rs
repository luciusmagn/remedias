//! Modul obsahující věci týkající se autentifikace
use crate::models::{User, Instance, Role};
use crate::Database;

use uuid::Uuid;
use serde::{Deserialize, Serialize};

use rocket::Outcome;
use rocket::Request;
use rocket::http::Status;
use rocket::request::FromRequest;

use chrono::Utc;

use reqwest;
use reqwest::header::CONTENT_TYPE;
use reqwest::header::AUTHORIZATION;

use std::fmt;
use std::str::FromStr;
use std::option::NoneError;
use std::marker::PhantomData;

/// JWT pro autorizaci atd.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AuthToken<T> {
	/// Issuer JWT - remedias
	pub iss:       String,
	/// Doba vypršení
	pub exp:       i64,
	/// Identifikátor uživatele
	pub id:        Uuid,
	/// Přezdívka uživatele, lze využít k autentifikaci
	pub nick:      String,
	/// Čitelné jméno uživatele
	pub display:   String,
	/// Email uživatele
	pub email:     String,
	/// Potvrzenost účtu
	pub confirmed: bool,
	/// Stránky, ke kterým se uživatel přihlásil
	pub sites:     Vec<Uuid>,
	/// Modifikátory přístupu
	pub perms:     Vec<String>,
	/// Lokace uživatele
	pub location:  Option<String>,
	/// Datum narození
	pub birthdate: String,
	_m:            PhantomData<T>,
}

impl<T> AuthToken<T> {
	/// Vytvoří nový authtoken z usera
	pub fn new(src: User) -> AuthToken<T> {
		let now = Utc::now().timestamp() + (69 * 60);

		AuthToken {
			iss:       "Meedias".to_string(),
			exp:       now,
			id:        src.id,
			nick:      src.nick,
			display:   src.display,
			email:     src.email,
			confirmed: src.confirmed,
			sites:     src.sites,
			perms:     src.perms,
			location:  src.location,
			birthdate: src.birthdate,
			_m:        PhantomData,
		}
	}

	/// Zkonvertuje na jiný AuthToken
	pub fn convert<U: Perm>(self) -> AuthToken<U> {
		AuthToken {
			iss:       self.iss,
			exp:       self.exp,
			id:        self.id,
			nick:      self.nick,
			display:   self.display,
			email:     self.email,
			confirmed: self.confirmed,
			sites:     self.sites,
			perms:     self.perms,
			location:  self.location,
			birthdate: self.birthdate,
			_m:        PhantomData,
		}
	}

	/// Nastaví timestamp na vypršení tokenu
	pub fn exp(mut self, exp: i64) -> AuthToken<T> {
		self.exp = exp;
		self
	}

	/// Zkonvertuje token na json
	pub fn make(&self) -> serde_json::Value {
		serde_json::to_value(&self).unwrap()
	}
}

/// authentification context for GraphQL
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtContext<T> {
	/// embedded Jwt
	pub jwt: Option<AuthToken<T>>,
}

impl<T> juniper::Context for AuthToken<T> {}
impl<T> juniper::Context for JwtContext<T> {}

impl<'a, 'r, T> FromRequest<'a, 'r> for AuthToken<T>
where
	T: Perm,
{
	type Error = String;

	fn from_request(
		request: &'a Request<'r>,
	) -> rocket::request::Outcome<Self, Self::Error> {
		let keys: Vec<_> = request.headers().get("Authorization").collect();
		println!("lel");
		match keys.get(0).unwrap_or(&"").split(' ').nth(1) {
			Some(ref token) => {
				let header = rejwt::decode_segments(&token).unwrap().0;

				if let Some(key_id) = header.get("kid") {
					let db = Database::<User>::open().unwrap(); // todo

					let user_id = Uuid::parse_str(key_id.as_str().unwrap()).ok().unwrap();
					let key = db.read().get(&user_id).unwrap().pub_key; // // [TODO]: Description

					match rejwt::decode(&token, &key, rejwt::Algorithm::RS256) {
						Ok(t) => match serde_json::from_value::<AuthToken<T>>(t.1) {
							Ok(tok) => {
								let now = Utc::now().timestamp();

								println!("lel 2");
								if now > tok.exp {
									Outcome::Failure((
										Status::ImATeapot,
										"token has expired".to_string(),
									))
								} else if global_auth(&tok).is_some() || !T::is_global() {
									Outcome::Success(tok) // TOOD will have to use with_id now
								} else {
									Outcome::Failure((
										Status::Forbidden,
										"invalid credentials or insufficient perms"
											.to_string(),
									))
								}
							}
							Err(_) => Outcome::Failure((
								Status::UnprocessableEntity,
								"token contains invalid data".to_string(),
							)),
						},
						Err(_) => Outcome::Failure((
							Status::Unauthorized,
							"couldn't verify token".to_string(),
						)),
					}
				} else {
					Outcome::Failure((
						Status::BadRequest,
						"invalid JWT header - missing kid".to_string(),
					))
				}
			}
			x => {
				println!("{:?}", x);
				Outcome::Failure((
					Status::BadRequest,
					"invalid authorization header".to_string(),
				))
			}
		}
	}
}

/// Důvod pro autentifikaci
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Reason {
	/// Změna globálních tagů
	/// `tags_global`
	TagsGlobal,
	/// Změna tagů obsahu
	/// `tags`
	Tags,
	/// Přiřazování kategorií danému obsahu
	/// `content_categories`
	ContentCategories,
	/// Vytváření, změna kategorií
	/// `categories`
	Categories,
	/// Změna uživatelských modifikátorů přístupu lokálních instanci
	/// `instance_perms`
	InstancePerms,
	/// Smazání instance
	/// `instance_delete`
	InstanceDelete,
	/// Změna vlastníků instance
	/// `instance_owners`
	InstanceOwners,
	/// Změna názvu instance
	/// `instance_name`
	InstanceName,
	/// Vytvoření nové instance
	/// `instance_create`
	InstanceCreate,
	/// Vytvoření/smazání mapy ID -> Slug
	/// `map`
	Map,
	/// Smazání uživatele (lokální)
	/// `user_delete_local`
	UserDeleteLocal,
	/// Kompletní smazání uživatele (globální)
	/// `user_delete`
	UserDeleteGlobal,
	/// Tvorba a změna obsahu
	/// `content`
	Content,
	/// Tvorba a změna stránek,
	/// resp. fragmentů
	/// `page`
	Page,
	/// Oprávnění pro adminy Meediasu
	/// Ignoruje všechna ostatní oprávnění
	/// `i_can_do_what_i_want`
	ICanDoWhatIWant,
	/// Použito pro automatické přihlášení
	/// před odesláním konfirmačního emailu
	/// `confirm`
	Confirm,
	/// Získání informací o sobě sama
	/// `me`
	Me,
	/// Reklamy
	/// `ads`
	Ads,
	/// Změní jiného uživatele (globálně)
	/// `user_modify`
	UserModify,
	/// Úkoly
	/// `tasks`
	Tasks,
	/// Změna uživatelských rolí
	/// `roles`
	Roles,
	/// Změna skupin
	/// `groups`
	Groups,
}

impl Reason {
	/// Vrací true, pokud je funkce globální
	pub fn is_global(&self) -> bool {
		use self::Reason::*;

		match self {
			Confirm | UserDeleteGlobal | ICanDoWhatIWant | Me | Ads | UserModify
			| InstanceCreate => true,
			_ => false,
		}
	}

	/// is local admin
	pub fn is_local_admin(&self) -> bool {
		use self::Reason::*;

		match self {
			Page | Content | Roles | Tasks | UserDeleteLocal | InstancePerms
			| InstanceName | InstanceOwners | Categories | ContentCategories | Tags
			| TagsGlobal | Map | Groups => true,
			_ => false,
		}
	}
}

#[allow(missing_docs)]
#[rustfmt::skip]
pub mod reason {
	#[derive(Clone)] pub struct TagsGlobal;
	#[derive(Clone)] pub struct Tags;
	#[derive(Clone)] pub struct Tasks;
	#[derive(Clone)] pub struct ContentCategories;
	#[derive(Clone)] pub struct Categories;
	#[derive(Clone)] pub struct InstancePerms;
	#[derive(Clone)] pub struct InstanceDelete;
	#[derive(Clone)] pub struct InstanceOwners;
	#[derive(Clone)] pub struct InstanceName;
	#[derive(Clone)] pub struct InstanceCreate;
	#[derive(Clone)] pub struct Map;
	#[derive(Clone)] pub struct UserDeleteLocal;
	#[derive(Clone)] pub struct UserDeleteGlobal;
	#[derive(Clone)] pub struct Content;
	#[derive(Clone)] pub struct Page;
	#[derive(Clone)] pub struct ICanDoWhatIWant;
	#[derive(Clone)] pub struct Confirm;
	#[derive(Clone)] pub struct Me;
	#[derive(Clone)] pub struct Ads;
	#[derive(Clone)] pub struct Roles;
	#[derive(Clone)] pub struct Groups;
	#[derive(Clone)] pub struct UserModify;

	pub trait Perm: Sized + Clone {
		fn is_global() -> bool {
			false
		}
		fn to_reason() -> super::Reason;
		fn with_id(tok: &AuthToken<Self>, id: &Uuid) -> bool {
			if tok.perms.contains(&"global:i_can_do_what_i_want".to_string()) || Self::is_global() && tok.perms.iter().any(|x| x == &format!("global:{}", Self::to_reason().to_string())) {
				true
			} else {
				super::auth(tok, id).is_some()
			}
		}
	}

	use uuid::Uuid;
	use super::AuthToken;

	impl_perm! {
		Confirm Me Ads UserModify InstanceCreate UserDeleteGlobal ICanDoWhatIWant
		TagsGlobal => false Tags => false ContentCategories => false
		Categories => false InstancePerms => false InstanceDelete => false UserDeleteLocal => false
		Map => false Content => false Page => false InstanceOwners => false InstanceName => false
		Tasks => false Roles => false Groups => false
	}
}

impl fmt::Display for Reason {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use self::Reason::*;

		match self {
			TagsGlobal => write!(f, "tags_global"),
			Tags => write!(f, "tags"),
			ContentCategories => write!(f, "content_categories"),
			Categories => write!(f, "categories"),
			InstancePerms => write!(f, "instance_perms"),
			InstanceDelete => write!(f, "instance_delete"),
			InstanceOwners => write!(f, "instance_owners"),
			InstanceName => write!(f, "instance_name"),
			InstanceCreate => write!(f, "instance_create"),
			UserDeleteLocal => write!(f, "user_delete_local"),
			UserDeleteGlobal => write!(f, "user_delete"),
			ICanDoWhatIWant => write!(f, "i_can_do_what_i_want"),
			UserModify => write!(f, "user_modify"),
			Confirm => write!(f, "confirm"),
			Content => write!(f, "content"),
			Page => write!(f, "page"),
			Map => write!(f, "map"),
			Ads => write!(f, "ads"),
			Me => write!(f, "me"),
			Tasks => write!(f, "tasks"),
			Roles => write!(f, "roles"),
			Groups => write!(f, "groups"),
		}
	}
}

impl FromStr for Reason {
	type Err = NoneError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		use self::Reason::*;

		match s {
			"tags_global" => Ok(TagsGlobal),
			"tags" => Ok(Tags),
			"content_categories" => Ok(ContentCategories),
			"categories" => Ok(Categories),
			"instance_perms" => Ok(InstancePerms),
			"instance_delete" => Ok(InstanceDelete),
			"instance_owners" => Ok(InstanceOwners),
			"instance_name" => Ok(InstanceName),
			"instance_create" => Ok(InstanceCreate),
			"user_delete_local" => Ok(UserDeleteLocal),
			"user_delete" => Ok(UserDeleteGlobal),
			"i_can_do_what_i_want" => Ok(ICanDoWhatIWant),
			"user_modify" => Ok(UserModify),
			"confirm" => Ok(Confirm),
			"content" => Ok(Content),
			"page" => Ok(Page),
			"map" => Ok(Map),
			"ads" => Ok(Ads),
			"me" => Ok(Me),
			"tasks" => Ok(Tasks),
			"roles" => Ok(Roles),
			"groups" => Ok(Groups),
			_ => Err(NoneError),
		}
	}
}

use self::reason::Perm;

/// Pokusí se autentifikovat uživatele pomocí
/// dodaných údajů. Pokud se autentifikace podaří,
/// vrací jeho záznam.
#[allow(clippy::blocks_in_if_conditions)]
pub fn auth<T: Perm>(info: &AuthToken<T>, instance_id: &Uuid) -> Option<User> {
	let reason = T::to_reason();

	if info.perms.iter().any(|x| {
		(x.starts_with(&instance_id.to_hyphenated().to_string())
			&& x.ends_with(&reason.to_string()))
			|| (x.starts_with("global")
				&& x.ends_with(&Reason::ICanDoWhatIWant.to_string()))
	}) && (info.confirmed || reason == Reason::Confirm)
	{
		let db = Database::<User>::open()?;

		db.read().get(&info.id)
	} else {
		let instances = Database::<Instance>::open()?;

		match instances.read().get(instance_id) {
			Some(i) =>
				if i.owners.contains(&info.id) {
					let db = Database::<User>::open()?;

					db.read().get(&info.id)
				} else {
					None
				},
			None => None,
		}
	}
}

/// Autentifikace pro operace netýkající se instancí
#[allow(clippy::blocks_in_if_conditions)]
pub fn global_auth<T: Perm>(info: &AuthToken<T>) -> Option<User> {
	let db = Database::<User>::open()?;
	let reason = T::to_reason();

	if !reason.is_global() {
		return None;
	}

	if info.perms.iter().cloned().fold(false, |a, x| {
		x.starts_with("global")
			&& (x.ends_with(&reason.to_string())
				|| x.ends_with(&Reason::ICanDoWhatIWant.to_string()))
			|| a
	}) {
		db.read().get(&info.id)
	} else {
		None
	}
}

fn recurse_role(user_id: Uuid, current: Role, info: &AuthToken<reason::Roles>) {
	if let Some(p) = current.parent {
		let db = Database::<Role>::open().unwrap();

		recurse_role(user_id, db.read().get(&p).unwrap(), info)
	}

	let client = reqwest::blocking::Client::new();

	client
		.delete(&format!(
			"http://localhost:8000/{}/user/{}/perms",
			current.instance.to_hyphenated().to_string(),
			user_id.to_hyphenated().to_string()
		))
		.header(CONTENT_TYPE, "application/json")
		.header(AUTHORIZATION, format!("Bearer {}", info.make()))
		.body(serde_json::to_string(&current.perms).unwrap())
		.send()
		.unwrap();

	client
		.patch(&format!(
			"http://localhost:8000/{}/user/{}/perms",
			current.instance.to_hyphenated().to_string(),
			user_id.to_hyphenated().to_string()
		))
		.header(CONTENT_TYPE, "application/json")
		.header(AUTHORIZATION, format!("Bearer {}", info.make()))
		.body(serde_json::to_string(&current.perms).unwrap())
		.send()
		.unwrap();
}

/// Aktualizuje oprávnění všech uživatelů, kterých se týká změna rolí
pub fn update_users_with_role(role_id: Uuid, info: &AuthToken<reason::Roles>) {
	let affected_users = {
		let db = Database::<User>::open().unwrap();

		db.read()
			.iter()
			.filter(|(_, x)| x.roles.contains(&role_id))
			.map(|(_, x)| (x.id, x.roles))
	};

	for (user_id, rls) in affected_users {
		let db = Database::<Role>::open().unwrap();

		rls.iter().for_each(|x| recurse_role(user_id, db.read().get(x).unwrap(), info));
	}
}
