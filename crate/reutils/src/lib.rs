//! Tento modul obsahuje utilitní funkce.
//! Původně byly rozházeny do `api` modulů,
//! ale v rámci organizace všechny přesunuty
//! sem.
#![feature(try_trait)]
#![deny(missing_docs, unused_extern_crates)]
extern crate uuid;
extern crate regex;
#[macro_use]
extern crate lazy_static;

extern crate relgr;
extern crate redb as db;

use relgr::log::{warn, info};
use crate::db::{Database, Instance};

use uuid::Uuid;
use regex::Regex;

use std::env;
use std::path::PathBuf;

pub use db::auth;
pub use db::auth::{Reason, auth, global_auth, AuthToken, reason, update_users_with_role};

lazy_static! {
	static ref DATUM_REGEX: Regex = Regex::new(r#"\d{4}-\f{2}-\f{2}"#).unwrap();
}

/// Kontrola, jestli daná instance existuje
pub fn instance_exists(instance_id: &Uuid) -> bool {
	let db = Database::<Instance>::open().unwrap();

	let string = instance_id.to_hyphenated().to_string();
	info!("checking if instance '{}' exists", string);

	let res = db.read().get(instance_id).is_some();

	if res {
		true
	} else {
		let string = instance_id.to_hyphenated().to_string();
		warn!("instance '{}' doesn't exist", string);
		false
	}
}

/// Přidá další složku do proměnné prostředí PATH
pub fn append_path(path: &str) -> Result<(), env::JoinPathsError> {
	if let Some(p) = env::var_os("PATH") {
		let mut paths = env::split_paths(&p).collect::<Vec<_>>();
		paths.push(PathBuf::from(path));
		let new_path = env::join_paths(paths)?;
		env::set_var("PATH", &new_path);
	}
	Ok(())
}
