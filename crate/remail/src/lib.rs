//! Modul obsahující vše, co se týká emailů.
//!
//! A toho moc není...
#![deny(missing_docs, unused_extern_crates)]

extern crate lettre;
extern crate lettre_email;

use lettre_email::EmailBuilder;
use lettre::{
	Transport, SmtpClient, SmtpTransport,
	smtp::{
		ClientSecurity,
		extension::ClientId,
		ConnectionReuseParameters,
		authentication::{Credentials, Mechanism},
	},
};

/// Obsahuje konfirmační email
pub mod confirmation {
	include!("../../../misc/emails/confirmation.rs");
}
/// Obsahuje zprávu o změně emailu
pub mod changed {
	include!("../../../misc/emails/changed.rs");
}

/// Výčet obsahující potenciální chyby vzniklé při odesílání emailů
#[derive(Debug, Clone)]
pub enum MailError {
	/// Chyby při komponování emailu
	BuildError(String),
	/// Chybu při vytváření klienta a odesílání emailů
	SmtpError(String),
}

impl MailError {
	/// Zabalí MailError -> Result<T, MailError>
	pub fn result<T>(self) -> Result<T, MailError> {
		Err(self)
	}
}

/// Posílá konfirmační email
pub fn send_mail(
	link: &str,
	destination: (&str, &str),
) -> Result<lettre::smtp::response::Response, MailError> {
	match EmailBuilder::new()
		.to(destination)
		.from("meedias@meedias.cz")
		.subject(confirmation::SUBJECT)
		.text(confirmation::MESSAGE.replace("{{link}}", link))
		.build()
	{
		Ok(email) => match SmtpClient::new("smtp.meedias.cz:587", ClientSecurity::None) {
			Ok(mailer) => match SmtpTransport::new(
				mailer
					.hello_name(ClientId::Domain("stmp.meedias.cz".to_string()))
					.credentials(Credentials::new(
						"meedias@meedias.cz".to_string(),
						"NYqU((P%N7".to_string(),
					))
					.smtp_utf8(true)
					.authentication_mechanism(Mechanism::Login)
					.connection_reuse(ConnectionReuseParameters::ReuseUnlimited),
			)
			.send(email.into())
			{
				Ok(o) => Ok(o),
				Err(e) => MailError::SmtpError(e.to_string()).result(),
			},
			Err(e) => MailError::SmtpError(e.to_string()).result(),
		},
		Err(e) => MailError::BuildError(e.to_string()).result(),
	}
}

/// Pošle zprávu uživateli určenému destinací
pub fn send_message(
	msg: &str,
	subject: &str,
	destination: (&str, &str),
) -> Result<lettre::smtp::response::Response, MailError> {
	match EmailBuilder::new()
		.to(destination)
		.from("meedias@meedias.cz")
		.subject(subject)
		.text(msg)
		.build()
	{
		Ok(email) => match SmtpClient::new("smtp.meedias.cz:587", ClientSecurity::None) {
			Ok(mailer) => match SmtpTransport::new(
				mailer
					.hello_name(ClientId::Domain("stmp.meedias.cz".to_string()))
					.credentials(Credentials::new(
						"meedias@meedias.cz".to_string(),
						"NYqU((P%N7".to_string(),
					))
					.smtp_utf8(true)
					.authentication_mechanism(Mechanism::Login)
					.connection_reuse(ConnectionReuseParameters::ReuseUnlimited),
			)
			.send(email.into())
			{
				Ok(o) => Ok(o),
				Err(e) => MailError::SmtpError(e.to_string()).result(),
			},
			Err(e) => MailError::SmtpError(e.to_string()).result(),
		},
		Err(e) => MailError::BuildError(e.to_string()).result(),
	}
}

// /// Pošle zprávu uživateli určenému destinací
// pub fn send_mail(_link: &str, _destination: (&str, &str)) -> ! {
// 	unimplemented!()
// }
//
// /// Pošle zprávu uživateli určenému destinací
// pub fn send_message(_msg: &str, _subject: &str, _destination: (&str, &str)) -> ! {
// 	unimplemented!()
// }
