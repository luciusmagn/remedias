//! Modul implementující univerzální typ pro odpověď na API požadavek
//! a automatické konverze typů s ním spojené.

use sled;
use rocket;
use serde_json;
use rocket::Request;
use rocket::response::Responder;
use serde::{Deserialize, Serialize};
use rocket::http::{Status, ContentType};

use std::ops::Try;
use std::io::Cursor;
use std::convert::AsRef;
use std::string::ToString;
use std::option::NoneError;
use std::marker::PhantomData;

/// Response typ, vyjadřuje všechny (momentálně)
/// možné odpovědi, na API požadavky
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ResponseVariants {
	/// Standardní odpověď, pokud byl požadavek validní
	Ok,
	/// Odpověď, pokud nebyl daný objekt nalezen,
	/// tato varianta je také výsledkem, pokud se použije
	/// try operátor (`?`) na `None`
	///
	/// Tato funkcionalita je důvodem, proč je na mnoha
	/// místech v kódu předčasně ukončen běh funkce pomocí
	/// `None?`
	NotFound,
	/// Kontejner pro JSON odpovědi, odpovídá se statusem OK
	Json(String),
	/// Vraceno při nesprávné autentifikaci
	Forbidden(String),
	/// Vraceno, pokud nedošlo k žádným změnám, často výsledkem
	/// některých chybných požadavků
	NotModified(String),
	/// Kompletně chybné požadavky
	BadRequest(String),
	/// Když je něco asi špatně na naší straně, např.
	/// se nedaří spustit mikroservery
	InternalServerError(String),
}

/// Response typ, vyjadřuje všechny (momentálně)
/// možné odpovědi, na API požadavky
/// - toto je typovaný kontejner
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	inner:  ResponseVariants,
	__type: PhantomData<T>,
}

impl ResponseVariants {
	/// Vytvoří typovaný [`Response˙] z netypovaného enumu
	pub fn wrap<T>(self) -> Response<T>
	where
		for<'a> T: Serialize + Deserialize<'a>,
	{
		Response { inner: self, __type: PhantomData }
	}
}

impl<T> AsRef<ResponseVariants> for Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	fn as_ref(&self) -> &ResponseVariants {
		&self.inner
	}
}

impl<T> Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	/// Umožní přístup k internímu enumu s daty
	pub fn into_variant(self) -> ResponseVariants {
		self.inner
	}

	/// Automaticky se pokusí zkonvertovat daný objekt na JSON
	/// Jediným požadavkem je, aby objekt implementoval obě `serde` traity
	pub fn json(src: &T) -> Result<Response<T>, serde_json::error::Error> {
		Ok(ResponseVariants::Json(serde_json::to_string(&src)?).wrap())
	}

	/// Automaticky se pokusí zkonvertovat JSON an objekt daného typu,
	/// vrací serde chybu, pokud se deserializace nepodařila a Ok(None),
	/// pokud `self` není varianta [`Response::Json`]
	///
	/// Požadavky jsou stejné jako u [`Response::json`]
	pub fn deser(&self) -> Result<Option<T>, serde_json::error::Error> {
		if let ResponseVariants::Json(ref s) = self.as_ref() {
			Ok(serde_json::from_str(s)?)
		} else {
			Ok(None)
		}
	}

	/// Pomocná funkce pro napodobení chování `Option`,
	/// počítáme, že všechny varianty kromě [`Response::Json`]
	/// jsou de facto ekvivalentní `None`
	pub fn is_none(&self) -> bool {
		if let ResponseVariants::Json(_) = self.as_ref() {
			false
		} else {
			true
		}
	}

	/// return true if bad status
	pub fn is_err(&self) -> bool {
		match self.as_ref() {
			ResponseVariants::Json(_)
			| ResponseVariants::Ok
			| ResponseVariants::NotModified(_) => false,
			ResponseVariants::NotFound
			| ResponseVariants::Forbidden(_)
			| ResponseVariants::InternalServerError(_)
			| ResponseVariants::BadRequest(_) => true,
		}
	}

	/// inverted `is_err`
	pub fn is_ok(&self) -> bool {
		!self.is_err()
	}

	/// Další Option<T> mimikry. Pokud je `self` varianty [`Response::Json`]
	/// prožene sebe sama predikátem `f`, jinak se jedná o `no-op`, kdy  [`Response`]
	/// vrací sebe-sama
	pub fn and_then<U, F: FnOnce(Response<T>) -> Response<U>>(self, f: F) -> Response<U>
	where
		for<'b> U: Serialize + Deserialize<'b>,
	{
		match self.as_ref() {
			ResponseVariants::Json(_) => f(self),
			ResponseVariants::Ok => Response::ok(),
			ResponseVariants::NotFound => Response::not_found(),
			ResponseVariants::NotModified(msg) => Response::not_modified(msg),
			ResponseVariants::Forbidden(msg) => Response::forbidden(msg),
			ResponseVariants::InternalServerError(msg) =>
				Response::internal_server_error(msg),
			ResponseVariants::BadRequest(msg) => Response::bad_request(msg),
		}
	}

	/// Pomocná funkce pro zjednodušené tvoření [`ResponseVariants::BadRequest`] jako [`Response`]
	pub fn bad_request<S: ToString>(msg: S) -> Response<T> {
		ResponseVariants::BadRequest(msg.to_string()).wrap()
	}

	/// Pomocná funkce pro zjednodušené tvoření [`ResponseVariants::NotModified`] jako [`Response`]
	pub fn not_modified<S: ToString>(msg: S) -> Response<T> {
		ResponseVariants::NotModified(msg.to_string()).wrap()
	}

	/// Pomocná funkce pro zjednodušené tvoření [`ResponseVariants::Forbidden`] jako [`Response`]
	pub fn forbidden<S: ToString>(msg: S) -> Response<T> {
		ResponseVariants::Forbidden(msg.to_string()).wrap()
	}

	/// Vytvoří [`ResponseVariants::Ok`] jako [`Response`]
	pub fn ok() -> Response<T> {
		ResponseVariants::Ok.wrap()
	}

	/// Vytvoří [`ResponseVariants::NotFound`] jako [`Response`]
	pub fn not_found() -> Response<T> {
		ResponseVariants::NotFound.wrap()
	}

	/// Vytvoří [`ResponseVariants::InternalServerError`] jako [`Response`]
	pub fn internal_server_error<S: ToString>(msg: S) -> Response<T> {
		ResponseVariants::InternalServerError(msg.to_string()).wrap()
	}
}

impl<'a, T> Responder<'a> for Response<T>
where
	for<'b> T: Serialize + Deserialize<'b>,
{
	/// Implementace Response jako responderu pro Rocket, umožňuje, aby
	/// endpointové funkce přímo vraceli Response
	fn respond_to(self, _: &Request) -> rocket::response::Result<'a> {
		match self.into_variant() {
			ResponseVariants::Ok => rocket::Response::build()
				.status(Status::Ok)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new("\"\""))
				.ok(),
			ResponseVariants::NotModified(msg) => rocket::Response::build()
				.status(Status::NotModified)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new(format!("\"{}\"", msg)))
				.ok(),
			ResponseVariants::NotFound => rocket::Response::build()
				.status(Status::NotFound)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new("\"object not found\"".to_string()))
				.ok(),
			ResponseVariants::BadRequest(msg) => rocket::Response::build()
				.status(Status::BadRequest)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new(format!("\"{}\"", msg)))
				.ok(),
			ResponseVariants::Forbidden(msg) => rocket::Response::build()
				.status(Status::Forbidden)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new(format!("\"{}\"", msg)))
				.ok(),
			ResponseVariants::InternalServerError(msg) => rocket::Response::build()
				.status(Status::InternalServerError)
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new(format!("\"{}\"", msg)))
				.ok(),
			ResponseVariants::Json(json) => rocket::Response::build()
				.header(ContentType::new("application", "json"))
				.sized_body(Cursor::new(json))
				.ok(),
		}
	}
}

impl<T> Try for Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	type Error = Response<T>;
	type Ok = Response<T>;

	/// Konvertuje Response na `Result<Response, Response>`.
	/// Varianta `Ok(_)` je použita, pokud Response není
	/// varianty [`Response::NotFound`]
	fn into_result(self) -> Result<Response<T>, Response<T>> {
		match self.into_variant() {
			ResponseVariants::NotFound => Err(Response::not_found()),
			x => Ok(x.wrap()),
		}
	}

	fn from_ok(_: Response<T>) -> Self {
		Response::ok()
	}

	fn from_error(_: Response<T>) -> Self {
		Response::not_found()
	}
}

impl<T> From<Option<T>> for Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	/// Konvertuje Option<T> na Response
	fn from(o: Option<T>) -> Response<T> {
		match o {
			None => Response::not_found(),
			Some(_) => Response::ok(),
		}
	}
}

impl<T> From<NoneError> for Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	/// Umožňuje `None?` a podobně
	fn from(_: NoneError) -> Response<T> {
		Response::not_found()
	}
}

impl<T> From<sled::Error> for Response<T>
where
	for<'a> T: Serialize + Deserialize<'a>,
{
	fn from(e: sled::Error) -> Response<T> {
		Response::internal_server_error(e.to_string())
	}
}
