//! Knihovna obsahující pomocné typy pro endpointy
#![feature(try_trait, proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]
extern crate serde_json;
#[macro_use]
extern crate rocket;
extern crate serde;
extern crate uuid;
extern crate sled;

use rocket::Request;
use rocket::{Data, Outcome::*};
use rocket::http::{Status, RawStr};
use rocket::request::FromFormValue;
use rocket::data::{self, FromDataSimple};

use uuid::Uuid;
use serde::{Serialize, Deserialize};

use std::io::Read;
use std::ops::Deref;
use std::convert::AsRef;

// Macros,
// due to macros being the only
// type of item dependent on position,
// we need to put them at the very top,
// before module declarations
#[macro_use]
pub mod macros;

pub mod response;
pub use crate::response::Response;

/// Struktura využívaná pro přihlášení uživatele
/// /login endpointy po zaslání vrací nový token
#[derive(Clone, Debug, Deserialize, Serialize, Default)]
pub struct AuthLogin {
	/// Jméno uživatele
	pub name:  Option<String>,
	/// Email
	pub email: Option<String>,
	/// heslo
	pub pass:  String,
}

impl_from_data! { AuthLogin }

/// Obaluje string jako JSON input data
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct InputString(pub String);

/// Obaluje UUID jako JSON input data
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct InputUuid(pub Uuid);

/// Obaluje UUID jako JSON input data
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct InputList(pub Vec<String>);

impl_trifecta! {
	InputList   => Vec<String>, Vec<String>
	InputString => str, String
	InputUuid   => Uuid, Uuid
}

impl<'v> FromFormValue<'v> for InputString {
	type Error = &'v RawStr;

	fn from_form_value(form_value: &'v RawStr) -> Result<InputString, &'v RawStr> {
		Ok(InputString(form_value.as_str().to_owned()))
	}
}

/// not found
#[catch(404)]
pub fn not_found() -> &'static str {
	"\"the resource does not exist\""
}

/// bad request
#[catch(400)]
pub fn bad_request() -> &'static str {
	"\"the request is invalid\""
}

/// forbidden
#[catch(403)]
pub fn forbidden() -> &'static str {
	"\"you are not allowed to do that\""
}

/// internal server error
#[catch(500)]
pub fn ise() -> &'static str {
	"\"the server encountered an error\""
}

/// teapot
#[catch(417)]
pub fn teapot() -> &'static str {
	"\"you made a fucky wucky\""
}

/// invalid entity
#[catch(422)]
pub fn invalid_entity() -> &'static str {
	"\"incorrect data format\""
}
