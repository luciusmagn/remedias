//! Modul obsahující makra pro zjednodušené odpovědi
//! Typ Response je mnohem více intuitivní než Option<status::Custom<http::Status, Json<T>>>
//! bylo, takže tyto makra nejsou úplně nutná, byť zjednodušují práci

/// Makro pro odpověď `200 OK`
#[macro_export]
macro_rules! ok {
	() => {{
		reapi::Response::ok()
		}};
	($arg:expr) => {{
		reapi::Response::json(&$arg)
			.unwrap_or(reapi::Response::internal_server_error("unknown error"))
		}};
}

/// Makro pro špatné požadavky, odpověď `400 Bad Request`
#[macro_export]
macro_rules! bad_request {
	($msg:expr) => {{
		return reapi::Response::bad_request(&$msg);
		}};
}

/// Makro pro požadavky, na které nemá uživatel oprávnění `403 Forbidden`
#[macro_export]
macro_rules! forbidden {
	($msg:expr) => {{
		return reapi::Response::forbidden(&$msg);
		}};
}

/// Makro pro sadu testů
#[macro_export]
macro_rules! test_set {
	{
		$(fn $name:ident ( $($arg:ident:$typ:ident $(,)* )* ) $code:block)*
	} => {
		use std::fs::File;
		use std::io::Read;

		$(
			#[test]
			pub fn $name ($($arg:$typ,)*) {
				$code
			}
		)*
	}
}

/// Vygeneruje implementaci traity FromDataSimple pro daný typ
#[macro_export]
macro_rules! impl_from_data {
	{$($tp:ty)*} => {$(
		impl FromDataSimple for $tp {
			type Error = String;

			// TODO add limits
			fn from_data(_: &Request, data: Data) -> data::Outcome<Self, Self::Error> {
				// Read the data into a String.
				let mut string = String::new();
				if let Err(e) = data.open().read_to_string(&mut string) {
					return Failure((Status::InternalServerError, e.to_string()));
				}

				match serde_json::from_str(&string) {
					Ok(s) => Success(s),
					Err(_) => Failure((Status::UnprocessableEntity, "invalid JSON".into()))
				}
			}
		}
	)*}
}

/// Vygeneruje FromDataSimple, AsRef a Deref implmenetaci pro daný typ, 1. typ 2. asref typ 3. deref typ
#[macro_export]
macro_rules! impl_trifecta {
	{$($tp:ty => $asref:ty, $deref:ty)*} => {$(
		impl AsRef<$asref> for $tp {
			fn as_ref(&self) -> &$asref {
				&self.0
			}
		}

		impl Deref for $tp {
			type Target = $deref;

			fn deref(&self) -> &Self::Target {
				&self.0
			}
		}

		impl_from_data! { $tp }
	)*}
}
