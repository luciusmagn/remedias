extern crate uuid;
extern crate redb;
extern crate chrono;

use uuid::Uuid;
use chrono::Utc;
use redb::{Table, Database};
use serde::{Serialize, Deserialize};

use std::collections::HashSet;

/// Model obsahu, tj. článku
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Content {
	/// Identifikátor obsahu
	pub id:              Uuid,
	/// Vlastnící instance
	pub instance:        Uuid,
	/// UUID autora
	pub author:          Uuid,
	/// Modifikátory přístupu
	pub perms:           Vec<String>,
	/// Čitelný název / Nadpis článku
	pub name:            String,
	/// Obsah
	pub contents:        String,
	/// Tagy přiřazené článku
	pub tags:            Vec<String>,
	/// UUID kategorií, do kterých je zařazen
	pub categories:      HashSet<Uuid>,
	/// Datum článku (asi napsání?), mělo by být ve formátu YYYY-MM-DD
	pub datum:           String,
	/// Lokace
	pub location:        Option<String>,
	/// Řetězec změn autorů
	pub changes:         Vec<String>,
	/// Počet zobrazení
	pub views:           i32,
	/// Popisek
	pub description:     Option<String>,
	/// Url přidruženého obrázku
	pub image_url:       Option<String>,
	/// Published
	pub published:       bool,
	/// Datum publikace
	pub published_datum: Option<String>,
	/// Id úkolu
	pub task_id:         Option<Uuid>,
	/// group
	pub group:           Option<Uuid>,
	/// slug
	pub slug:            Option<String>,
}

/// Model obsahu, tj. článku
#[allow(warnings)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct NewContent {
	/// Identifikátor obsahu
	pub id:           Uuid,
	/// Vlastnící instance
	pub instance:     Uuid,
	/// UUID autora
	pub author:       Uuid,
	/// Modifikátory přístupu
	pub perms:        Vec<String>,
	/// Čitelný název / Nadpis článku
	pub name:         String,
	/// Obsah
	pub contents:     String,
	/// Tagy přiřazené článku
	pub tags:         Vec<String>,
	/// UUID kategorií, do kterých je zařazen
	pub categories:   HashSet<Uuid>,
	/// Lokace
	pub location:     Option<String>,
	/// Řetězec změn autorů
	pub changes:      Vec<String>,
	/// Počet zobrazení
	pub views:        i32,
	/// Popisek
	pub description:  Option<String>,
	/// Url přidruženého obrázku
	pub image_url:    Option<String>,
	/// Published
	pub published:    bool,
	/// Id úkolu
	pub task_id:      Option<Uuid>,
	/// group
	pub group:        Option<Uuid>,
	/// slug
	pub slug:         Option<String>,
	/// Datum plánované publikace
	pub publish_at:   Option<String>,
	/// Datum faktické publikace
	pub published_at: Option<String>,
	/// Datum vytvoření
	pub created_at:   String,
	/// Datum aktualizace
	pub updated_at:   String,
}

impl Table for Content {
	type Key = Uuid;
	type Value = Content;

	fn name() -> &'static str {
		"content"
	}
}

fn main() {
	let ids = Database::<Content>::open()
		.unwrap()
		.read()
		.iter()
		.map(|(id, _)| id)
		.collect::<Vec<Uuid>>();

	for i in ids {
		// TODO handle Result properly
		let _ = Database::<Content>::open()
			.unwrap()
			.write()
			.update_transform::<_, Content, NewContent, _>(i, |c| {
				c.map(|x| NewContent {
					id:           x.id,
					instance:     x.instance,
					author:       x.author,
					perms:        x.perms,
					name:         x.name,
					contents:     x.contents,
					tags:         x.tags,
					categories:   x.categories,
					location:     x.location,
					changes:      x.changes,
					views:        x.views,
					description:  x.description,
					image_url:    x.image_url,
					published:    x.published,
					task_id:      x.task_id,
					group:        x.group,
					slug:         x.slug,
					created_at:   x.datum,
					published_at: x.published_datum,
					publish_at:   None,
					updated_at:   Utc::now().to_rfc3339(),
				})
			});
	}
}
