extern crate log;

use std::thread;
use std::sync::Mutex;
use std::fs::{OpenOptions, File};

use names::Generator;

thread_local! {
	pub static THREAD_ID: String = {
		let mut generator = Generator::default();
		generator.next().unwrap().to_string()
	};
}

lazy_static! {
	pub static ref FILE: Mutex<File> = Mutex::new(
		OpenOptions::new()
			.append(true)
			.create(true)
			.open(&format!("remedias.log"))
			.expect("can't get log file")
	);
}

#[cfg(feature = "color")]
pub fn print(record: &log::Record) {
	use termcolor::{WriteColor, BufferWriter, ColorChoice, ColorSpec, Color};
	use std::io::Write;

	let level = record.level().clone();
	let args = record.args().to_string();
	let file = record.module_path().unwrap_or("UNKNOWN").to_string();
	let line = record.line().unwrap_or(0);
	let mut thread = "NO_THREAD".into();

	THREAD_ID.with(|thread_id| {
		thread = thread_id.clone();
	});
	thread::spawn(move || {
		let writer = BufferWriter::stdout(ColorChoice::Auto);
		let mut buffer = writer.buffer();
		let level_color = match level {
			log::Level::Error => Color::Red,
			log::Level::Warn => Color::Yellow,
			log::Level::Info => Color::Green,
			log::Level::Debug => Color::Cyan,
			log::Level::Trace => Color::Blue,
		};

		let _ = buffer.set_color(ColorSpec::new().set_fg(Some(level_color)));
		let _ = write!(&mut buffer, "{:<5} ", level);
		let _ = buffer.reset();

		let _ = buffer.set_color(ColorSpec::new().set_fg(Some(Color::Yellow)));
		let _ = write!(&mut buffer, "[{:<26}] ", thread);
		let _ = buffer.reset();

		#[cfg(feature = "timestamp")]
		{
			let _ = buffer.set_color(ColorSpec::new().set_fg(Some(Color::Magenta)));
			let _ = write!(&mut buffer, "[{}] ", super::get_date());
		}

		let _ = buffer.set_color(ColorSpec::new().set_fg(Some(Color::Ansi256(247))));
		let _ = write!(&mut buffer, " at {:>35}:{:>4} ", file, line,);
		let _ = buffer.reset();

		let _ =
			buffer.set_color(ColorSpec::new().set_fg(Some(Color::Rgb(255, 255, 255))));
		let _ = write!(&mut buffer, "- {}\n", args);
		let _ = buffer.reset();

		let _ = writer.print(&buffer);

		let mut f = FILE.lock().expect("poisoned");
		let _ = writeln!(
			f,
			"{:<5} [{:<26}] [{}] at {:>35}:{:>4} - {}",
			level,
			thread,
			super::get_date(),
			file,
			line,
			args,
		);
	});
}

#[cfg(not(target_arch = "wasm32"))]
#[cfg(not(feature = "color"))]
pub fn print(record: &log::Record) {
	#[cfg(feature = "timestamp")]
	{
		println!(
			"{:<5} [{}] - {} at {}:{}",
			record.level(),
			super::get_date(),
			record.args(),
			record.file().unwrap_or("UNKNOWN"),
			record.line().unwrap_or(0)
		);
	}

	#[cfg(not(feature = "timestamp"))]
	{
		println!(
			"{:<5} - {} at {}:{}",
			record.level(),
			record.args(),
			record.file().unwrap_or("UNKNOWN"),
			record.line().unwrap_or(0)
		);
	}
}
