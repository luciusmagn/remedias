extern crate log;
extern crate relgr;

#[test]
fn it_works() {
	relgr::Logger::print(
		&log::Record::builder()
			.args(format_args!("it works!"))
			.level(log::Level::Error)
			.build(),
	);
	relgr::Logger::print(
		&log::Record::builder()
			.args(format_args!("it works!"))
			.level(log::Level::Warn)
			.build(),
	);
	relgr::Logger::print(&log::Record::builder().args(format_args!("it works!")).build());
	relgr::Logger::print(
		&log::Record::builder()
			.args(format_args!("it works!"))
			.level(log::Level::Debug)
			.build(),
	);
	relgr::Logger::print(
		&log::Record::builder()
			.args(format_args!("it works!"))
			.level(log::Level::Trace)
			.build(),
	);
}
