//! # Remedias
//!
//! Remedias je centrální server sítě Meedias.
//! Spravuje landing page, endpointy API, hosting
//! statických souborů a administraci.
//!
//! V současnosti Remedias využívá sílu frameworku
//! Rocket pro web a Diesel jako ORM.
//!
//! Protože je Remedias poměrně velký projekt, je
//! pro jeho údržbu přiložen malý nástroj, `sentinel`.
//! Pro více informací si přečtěte jeho vlastní
//! dokumentaci.
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]

extern crate rocket_contrib;
extern crate rocket_cors;
#[macro_use]
extern crate rocket;

extern crate uuid;
extern crate chrono;
extern crate clokwerk;
extern crate command_macros;

// our crates
#[cfg(not(test))]
pub extern crate reapi;
#[cfg(test)]
#[macro_use]
extern crate reapi;
extern crate relgr;

extern crate statistics as _statistics;
extern crate category as _category;
extern crate instance as _instance;
extern crate content as _content;
extern crate query as _query;
extern crate tasks as _tasks;
extern crate roles as _roles;
extern crate user as _user;

extern crate reutils;
pub mod util {
	//! Tento modul obsahuje utilitní funkce.
	//! Původně byly rozházeny do `api` modulů,
	//! ale v rámci organizace všechny přesunuty
	//! sem.
	pub use reutils::*;
}

extern crate remail;
pub mod email {
	//! Modul obsahující vše, co se týká emailů.
	pub use remail::*;
}

extern crate redb;
pub mod db {
	//! Tento modul obsahuje vše, co se týká databáze,
	//! tj. modely, schéma a funkce pro komunikaci s Postgresem.
	//! V budoucnosti je v plánu tento modul generalizovat
	//! a přidat podporu pro SQLite (kvůli jednoduššímu testování)
	pub use redb::*;
}

pub extern crate restatic;

// TODO fix reponder docs
pub mod api;

#[cfg(test)]
mod tests;

use rocket::fairing::AdHoc;
use rocket_contrib::templates::Template;
use rocket_cors::{AllowedOrigins, AllowedHeaders};

use chrono::{DateTime, Utc, FixedOffset};

use clokwerk::{Scheduler, TimeUnits};
// Import week days and WeekDayø
use command_macros::command;

use std::env;
use std::thread;
use std::time::Duration;

use uuid::Uuid;

use relgr::log::info;
use redb::{Database, Content};

use std::str::FromStr;

/// Vytvoří novou instanci
#[allow(clippy::match_bool)]
pub fn init() -> rocket::Rocket {
	match relgr::init() {
		Ok(()) => eprintln!("successfully initialized logger"),
		Err(e) => eprintln!("logger failed: {}", e),
	}

	env::set_current_dir("covfefe").expect("perhaps a missing landing submodule?");

	command!(npm install).status().expect("failed to compile covfefe");
	command!(npm run build).status().expect("failed to build covfefe");
	command!(cp -a dist/. ../static/).status().expect("failed to covfefe");

	env::set_current_dir("..").unwrap();

	info!("lanching remedias");

	let cors = rocket_cors::CorsOptions {
		allowed_origins: AllowedOrigins::all(),
		allowed_methods: vec![
			"options", "delete", "patch", "trace", "post", "get", "put",
		]
		.into_iter()
		.map(|s| {
			FromStr::from_str(s)
				.unwrap_or_else(|_| unreachable!("HTTP methods must be correct"))
		})
		.collect(),
		allowed_headers: AllowedHeaders::all(),
		allow_credentials: true,
		..Default::default()
	}
	.to_cors()
	.unwrap();

	let mut scheduler = Scheduler::with_tz(chrono::Utc);

	scheduler.every(69.seconds()).run(|| {
		info!("running the auto-publish demon");
		let ids = Database::<Content>::open()
			.unwrap()
			.read()
			.iter()
			.filter(|(_, x)| {
				if x.publish_at.is_none() {
					return false; // no op
				}

				let now = Utc::now();
				let publish_at = chrono::DateTime::<FixedOffset>::parse_from_rfc3339(
					&x.publish_at.clone().unwrap(),
				)
				.unwrap_or_else(|_| {
					DateTime::<FixedOffset>::parse_from_str(
						"1983 Apr 13 12:09:14.274 +0000",
						"%Y %b %d %H:%M:%S%.3f %z",
					)
					.unwrap()
				});

				now > publish_at.with_timezone(&Utc)
			})
			.map(|(id, _)| id)
			.collect::<Vec<Uuid>>();

		for i in ids {
			// TODO handle Result properly
			let _ = Database::<Content>::open().unwrap().write().update::<_, Content, _>(
				i,
				|c| {
					c.map(|mut x| {
						let now = Utc::now();

						x.published = true;
						x.published_at = Some(now.to_rfc3339());
						x.updated_at = now.to_rfc3339();

						info!("published content with id [\"{}\"]", x.id.to_hyphenated());
						x
					})
				},
			);
		}
	});

	thread::spawn(move || loop {
		scheduler.run_pending();
		thread::sleep(Duration::from_millis(400));
		thread::yield_now();
	});

	rocket::ignite()
		.mount("/", restatic::landing_routes())
		.mount("/", api::statistics::routes())
		.mount("/", restatic::static_routes())
		.mount("/", api::instance::routes())
		.mount("/", api::category::routes())
		.mount("/", api::content::routes())
		.mount("/", api::user::routes())
		.mount("/", api::query::routes())
		.mount("/", api::tasks::routes())
		.mount("/", api::roles::routes())
		.register(catchers![
			reapi::ise,
			reapi::teapot,
			reapi::not_found,
			reapi::forbidden,
			reapi::bad_request,
			reapi::invalid_entity,
		])
		.attach(AdHoc::on_launch("Environment Switcher", |rocket| {
			match rocket.config().environment.is_dev() {
				true =>
					util::append_path("target/debug").unwrap_or_else(|_| unreachable!()),
				false =>
					util::append_path("target/release").unwrap_or_else(|_| unreachable!()),
			}
		}))
		.attach(Template::fairing())
		.attach(cors)
		.attach(AdHoc::on_launch("load finish", |_| {
			info!("remedias successfuly launched")
		}))
}
