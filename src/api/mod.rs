//! Tento modul obsahuje endpointy REST API
//! Meediasu.

pub use reapi::Response;

pub mod statistics;
pub mod category;
pub mod instance;
pub mod content;
pub mod query;
pub mod tasks;
pub mod roles;
pub mod user;
