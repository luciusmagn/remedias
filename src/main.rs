//! Modul spustitelného souboru

extern crate remedias;
extern crate dotenv;

pub fn main() {
	dotenv::dotenv().ok();
	remedias::init().launch();
}
