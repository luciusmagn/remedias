use redb::graphql::{UserRoot, UserSchema, user::UserMutations};
use reutils::{reason, AuthToken, auth::JwtContext};

use rocket::response::content;

/*
** GraphQL
*/

/// graphql get
#[get("/user?<request>")]
pub fn user(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&UserSchema::new(UserRoot, UserMutations), &JwtContext { jwt: tok })
}

/// graphql post
#[post("/user", data = "<request>")]
pub fn user_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&UserSchema::new(UserRoot, UserMutations), &JwtContext { jwt: tok })
}

/// graphql iql
#[get("/user/iql")]
pub fn user_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/user")
}
