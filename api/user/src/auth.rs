use rocket_contrib::uuid::Uuid;

use redb::{User, RUser, Database, NewEntry, NewEntryPartial, make_hash};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{AuthLogin, Response};
use rejwt::{encode, Algorithm};

use std::process::Command;

/*
** Endpoints for registration, confirmation, logging in and deletion
*/

/// Registrace uživatele globálně"
#[allow(unused_mut)]
#[post("/auth/register", format = "application/json", data = "<input>")]
pub fn register(input: RUser, mut db: Database<User>) -> Response<()> {
	if db
		.read()
		.iter()
		.filter(|(_, u)| u.nick == input.nick || u.email == input.email)
		.count() == 0
	{
		User::create(input)
			.and_modify(|mut x| {
				x.perms = vec!["global:confirm".into(), "global:me".into()]
			})
			.save()?;

		ok!()
	} else {
		bad_request!("user already exists")
	}
}

/// Potvrzení účtu uživatele"
#[get("/auth/confirm/<user_id>")]
pub fn confirm(user_id: Uuid, mut db: Database<User>) -> Response<()> {
	let res = db.write().update::<_, User, _>(&*user_id, |x| {
		if let Some(mut u) = x {
			u.confirmed = true;
			Some(u)
		} else {
			None
		}
	});

	match res {
		Ok(_) => ok!(),
		Err(_) => bad_request!("failed to confirm user"),
	}
}

// todo gut delet
/// Smaže uživatele
#[delete(
	"/global/user/<user_id>",
	format = "application/json",
	data = "<input>",
	rank = 4
)]
pub fn delete(
	user_id: Uuid,
	input: Option<AuthLogin>,
	mut db: Database<User>,
	info: AuthToken<reason::Me>,
) -> Response<()> {
	if info.id != *user_id
		&& login(input.unwrap_or_default(), Database::<User>::open().unwrap()).is_ok()
		|| reason::ICanDoWhatIWant::with_id(&info.convert(), &uuid::Uuid::new_v4())
	{
		forbidden!("you don't have the required permissions")
	}

	db.write().delete(*user_id).ok()?;

	ok!()
}

/// Odebere instanci od uživatele: 'smaže uživatele z instance'
///Také odebere veškeré modifikátory přístupu týkající se dané instance"
#[delete("/<instance_id>/user/<user_id>", format = "application/json")]
pub fn i_delete(
	instance_id: Uuid,
	user_id: Uuid,
	info: AuthToken<reason::Me>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::UserDeleteLocal::with_id(&info.clone().convert(), &*instance_id)
		|| info.id == *user_id
	{
		forbidden!("you don't have the required permissions")
	}

	db.write().update::<_, User, _>(*user_id, |c| {
		c.map(|mut x| {
			x.sites.retain(|e| e != &*instance_id);
			x.perms.retain(|e| !e.contains(&instance_id.to_hyphenated().to_string()));
			x
		})
	})?;

	ok!()
}

/// Globální přihlášení uživatele"
#[post("/auth/login", format = "application/json", data = "<input>")]
pub fn login(input: AuthLogin, db: Database<User>) -> Response<String> {
	let uhash = make_hash(&input.pass);

	let u = db.read().iter().find_map(|(_, x)| {
		if (input.name.as_ref().map_or(false, |n| &x.nick == n)
			|| input.email.as_ref().map_or(false, |m| &x.email == m))
			&& x.hash == uhash
		{
			Some(x)
		} else {
			None
		}
	})?;

	let header = json!({ "kid": u.id.to_hyphenated().to_string() });

	let body = AuthToken::<reason::Me>::new(u.clone()).make();

	ok!(encode(header, &u.priv_key, &body, Algorithm::RS256).unwrap())
}

/// Validace tokenu"
#[post("/auth/check", format = "application/json")]
pub fn check(info: AuthToken<reason::Me>, db: Database<User>) -> Response<String> {
	let u = db.read().iter().find(|(id, _)| id == &info.id).map(|(_, u)| u)?;

	let header = json!({ "kid": u.id.to_hyphenated().to_string() });
	let body = AuthToken::<reason::Me>::new(u.clone()).exp(info.exp).make();

	Some(encode(header, &u.priv_key, &body, Algorithm::RS256).unwrap()).into()
}

/// Validace tokenu"
#[post("/auth/invalidate", format = "application/json")]
pub fn invalidate(info: AuthToken<reason::Me>, mut db: Database<User>) -> Response<()> {
	let keys = String::from_utf8(
		Command::new("nim/keymaster").output().expect("sad story").stdout,
	)
	.unwrap()
	.split('|')
	.map(|x| x.to_string())
	.collect::<Vec<String>>();

	let (priv_key, pub_key) = (keys[0].to_string(), keys[1].to_string());

	let res = db.write().update::<_, User, _>(info.id, |x| {
		if let Some(mut u) = x {
			u.pub_key = pub_key.clone();
			u.priv_key = priv_key.clone();
			Some(u)
		} else {
			None
		}
	});

	match res {
		Ok(_) => ok!(),
		Err(_) => bad_request!("failed to invalidate token"),
	}
}

/// Obnova tokenu"
#[post("/auth/refresh", format = "application/json")]
pub fn refresh(info: AuthToken<reason::Me>, db: Database<User>) -> Response<String> {
	let u = db.read().iter().find(|(id, _)| id == &info.id).map(|(_, u)| u)?;

	let header = json!({ "kid": u.id.to_hyphenated().to_string() });
	let body = AuthToken::<reason::Me>::new(u.clone()).make();

	Some(encode(header, &u.priv_key, &body, Algorithm::RS256).unwrap()).into()
}

/// makes u big
#[post("/auth/i_am_god/uM0Cq6zrwgAZZ00Z", format = "application/json")]
pub fn i_am_god(info: AuthToken<reason::Me>, mut db: Database<User>) -> Response<String> {
	db.write().update::<_, User, _>(info.id, |c| {
		c.map(|mut x| {
			x.perms.push("global:i_can_do_what_i_want".to_string());
			x
		})
	})?;

	ok!()
}
