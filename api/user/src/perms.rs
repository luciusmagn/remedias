use rocket_contrib::uuid::Uuid;

use redb::{User, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputList};

/*
** User management endpoints - perms
*/

/// Vrátí oprávnění daného uživatele vůči dané instanci"
#[get("/<instance_id>/<user_id>/perms", rank = 3)]
pub fn get(
	instance_id: Uuid,
	user_id: Uuid,
	db: Database<User>,
) -> Response<Vec<String>> {
	ok!(db
		.read()
		.get(*user_id)?
		.perms
		.iter()
		.filter(|x| x.starts_with(&instance_id.to_hyphenated().to_string()))
		.cloned()
		.collect::<Vec<_>>())
}

/// Přepíše současné modifikátory přístupu daného uživatele"
#[post(
	"/<instance_id>/user/<user_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn post(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::InstancePerms>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::InstancePerms::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	db.write().update::<_, User, _>(*user_id, |c| {
		c.map(|mut x| {
			if let Some(l) = input.clone() {
				x.perms
					.retain(|x| !x.starts_with(&instance_id.to_hyphenated().to_string()));
				x.perms.extend(
					l.0.iter()
						.map(|x| format!("{}:{}", instance_id.to_hyphenated(), x))
						.collect::<Vec<_>>(),
				);
			}
			x.perms.dedup();
			x
		})
	})?;

	ok!()
}

/// Rozšíří seznam modifikátorů přístupu daného uživatele o dodaný list"
#[patch(
	"/<instance_id>/user/<user_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn patch(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::InstancePerms>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::InstancePerms::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	db.write().update::<_, User, _>(*user_id, |c| {
		c.map(|mut x| {
			if let Some(l) = input.clone() {
				x.perms.extend(
					l.0.iter()
						.map(|x| format!("{}:{}", instance_id.to_hyphenated(), x))
						.collect::<Vec<_>>(),
				)
			}
			x.perms.dedup();
			x
		})
	})?;

	ok!()
}

/// Smaže modifikátory přístupu, chování viz. categories_delete"
#[delete(
	"/<instance_id>/user/<user_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 5
)]
pub fn delete(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::InstancePerms>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::InstancePerms::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	db.write().update::<_, User, _>(*user_id, |c| {
		c.map(|mut x| {
			if let Some(l) = input.clone() {
				x.perms.retain(|x| {
					!l.0.iter()
						.map(|x| format!("{}:{}", instance_id.to_hyphenated(), x))
						.any(|y| y == *x)
				});
			} else {
				x.perms
					.retain(|x| x.starts_with(&instance_id.to_hyphenated().to_string()));
			}
			x.perms.dedup();
			x
		})
	})?;

	ok!()
}
