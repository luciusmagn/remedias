use rocket_contrib::uuid::Uuid;

use redb::{Role, User, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputList};

/*
** User management endpoints - roles
*/
/// Vrátí oprávnění daného uživatele vůči dané instanci"
#[get("/<instance_id>/<user_id>/roles", rank = 3)]
pub fn get(
	instance_id: Uuid,
	user_id: Uuid,
	db: Database<User>,
	roles: Database<Role>,
) -> Response<Vec<uuid::Uuid>> {
	ok!(db
		.read()
		.get(&*user_id)?
		.roles
		.iter()
		.filter(|x| roles.read().get(*x).filter(|r| r.instance == *instance_id).is_some())
		.cloned()
		.collect::<Vec<uuid::Uuid>>())
}

/// Přepíše současné modifikátory přístupu daného uživatele"
#[post(
	"/<instance_id>/user/<user_id>/roles",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn post(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*user_id).is_some() {
		db.write().update::<_, User, _>(*user_id, |c| {
			c.map(|mut x| {
				x.roles = input
					.clone()
					.map(|v| {
						v.0.iter()
							.map(|x| uuid::Uuid::parse_str(&x).unwrap())
							.collect::<Vec<_>>()
					})
					.unwrap_or_default();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("user doesn't exist")
	}
}

/// Rozšíří seznam modifikátorů přístupu daného uživatele o dodaný list"
#[patch(
	"/<instance_id>/user/<user_id>/roles",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn patch(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*user_id).is_some() {
		db.write().update::<_, User, _>(*user_id, |c| {
			c.map(|mut x| {
				x.roles.extend(
					input
						.clone()
						.map(|v| {
							v.0.iter()
								.map(|x| uuid::Uuid::parse_str(&x).unwrap())
								.collect::<Vec<_>>()
						})
						.unwrap_or_default(),
				);
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("user doesn't exist")
	}
}

/// Smaže modifikátory přístupu, chování viz. categories_delete"
#[delete(
	"/<instance_id>/user/<user_id>/roles",
	format = "application/json",
	data = "<input>",
	rank = 5
)]
pub fn delete(
	instance_id: Uuid,
	user_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<User>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*user_id).is_some() {
		db.write().update::<_, User, _>(*user_id, |c| {
			c.map(|mut x| {
				x.roles.retain(|p| {
					!input
						.clone()
						.map(|v| {
							v.0.iter()
								.map(|x| uuid::Uuid::parse_str(&x).unwrap())
								.collect::<Vec<_>>()
						})
						.unwrap_or_default()
						.contains(p)
				});
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("user doesn't exist")
	}
}
