//! Modul obsahující endpointy uživatelů
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_json;

extern crate redb;
extern crate rejwt;
extern crate reutils;
#[macro_use]
extern crate reapi;

use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{User, RUser, Database};
use reutils::{AuthToken, reason};
use reapi::{Response, InputString};

/// graphql
pub mod graphql;
/// perms
pub mod perms;
/// groups
pub mod groups;
/// roles
pub mod roles;
/// auth
pub mod auth;

/*
** User endpoints
*/

/// Vrací informace o daném uživateli"
#[get("/user/<user_id>", format = "application/json")]
pub fn get(
	user_id: Uuid,
	_info: AuthToken<reason::Me>,
	db: Database<User>,
) -> Response<User> {
	ok!(db.read().iter().find(|(id, _)| id == &*user_id).map(|(_, mut x)| {
		x.hash = "redacted".into();
		x.priv_key = "lol no".into();
		x
	})?)
}

/// Upraví uživatele"
#[patch("/user/<user_id>", format = "application/json", data = "<input>", rank = 3)]
pub fn patch(
	user_id: Uuid,
	input: RUser,
	_info: AuthToken<reason::Me>,
	mut db: Database<User>,
) -> Response<User> {
	let res = db.write().update::<_, User, _>(*user_id, |x| {
		if let Some(mut u) = x {
			u.nick = input.nick.clone();
			u.display = input.display.clone();
			u.birthdate = input.birthdate.clone();
			u.location = input.location.clone();
			Some(u)
		} else {
			None
		}
	});

	match res {
		Ok(_) => ok!(db.read().get(*user_id)?),
		Err(_) => bad_request!("couldn't patch user"),
	}
}

/// Vrací informace o autentifikovaném uživateli"
#[get("/me", format = "application/json")]
pub fn get_me(info: AuthToken<reason::Me>, db: Database<User>) -> Response<User> {
	ok!(db.read().iter().find_map(|(id, u)| if id == info.id {
		Some(u)
	} else {
		None
	})?)
}

/// Aktualizuje email uživatele
///	Invaliduje potvrzenost emailu"
// TODO extract email for better configuration
#[patch("/user/<user_id>/email", format = "application/json", data = "<input>", rank = 3)]
pub fn patch_email(
	user_id: Uuid,
	input: InputString,
	info: AuthToken<reason::Me>,
) -> Response<()> {
	todo!("emails are kinda gay")
}

/// returns routes
pub fn routes() -> Vec<Route> {
	routes![
		get,
		patch,
		get_me,
		patch_email,
		auth::register,
		auth::confirm,
		auth::delete,
		auth::i_delete,
		auth::login,
		auth::check,
		auth::invalidate,
		auth::refresh,
		auth::i_am_god,
		perms::get,
		perms::post,
		perms::patch,
		perms::delete,
		roles::get,
		roles::post,
		roles::patch,
		roles::delete,
		groups::get,
		groups::post,
		groups::patch,
		groups::delete,
		graphql::user,
		graphql::user_iql,
		graphql::user_post,
	]
}
