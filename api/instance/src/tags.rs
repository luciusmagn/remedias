use reutils::{AuthToken, reason::Perm, reason};
use reapi::{InputList, Response};
use redb::{Database, Instance};

use rocket_contrib::uuid::Uuid;
/*
** Globální tagy
*/

/// Přepíše původní globální tagy
#[post("/<instance_id>/tags/global", format = "application/json", data = "<input>")]
pub fn post(
	instance_id: Uuid,
	input: Option<InputList>,
	tok: AuthToken<reason::TagsGlobal>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::TagsGlobal::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	if db.read().iter().filter(|(id, _)| id == &*instance_id).count() != 0 {
		db.write().update::<_, Instance, _>(*instance_id, |c| {
			c.map(|mut x| {
				x.tags = input.clone().map(|v| v.0).unwrap_or_default();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("instance doesn't exist")
	}
}

/// Přidá další globální tagy k těm stávajícím, chování identické `categories_patch`
#[patch("/<instance_id>/tags/global", format = "application/json", data = "<input>")]
pub fn patch(
	instance_id: Uuid,
	input: Option<InputList>,
	tok: AuthToken<reason::TagsGlobal>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::TagsGlobal::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	if db.read().iter().filter(|(id, _)| id == &*instance_id).count() != 0 {
		db.write().update::<_, Instance, _>(*instance_id, |c| {
			c.map(|mut x| {
				x.tags.extend(input.clone().map(|v| v.0).unwrap_or_default());
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("instance doesn't exist")
	}
}

/// Smaže globální tagy, chování identické k `categories_delete`
#[delete("/<instance_id>/tags/global", format = "application/json", data = "<input>")]
pub fn delete(
	instance_id: Uuid,
	input: Option<InputList>,
	tok: AuthToken<reason::TagsGlobal>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::TagsGlobal::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	if db.read().iter().filter(|(id, _)| id == &*instance_id).count() != 0 {
		db.write().update::<_, Instance, _>(*instance_id, |c| {
			c.map(|mut x| {
				x.tags.retain(|p| {
					!input.clone().map(|v| v.0).unwrap_or_default().contains(p)
				});
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("instance doesn't exist")
	}
}
