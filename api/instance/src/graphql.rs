use redb::graphql::{InstanceRoot, InstanceSchema, instance::InstanceMutations};

use reutils::{reason, AuthToken, auth::JwtContext};

use rocket::response::content;

/*
** GraphQL
*/

/// graphql get
#[get("/instance?<request>")]
pub fn graphql(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&InstanceSchema::new(InstanceRoot, InstanceMutations), &JwtContext {
		jwt: tok,
	})
}

/// graphql post
#[post("/instance", data = "<request>")]
pub fn graphql_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&InstanceSchema::new(InstanceRoot, InstanceMutations), &JwtContext {
		jwt: tok,
	})
}

/// graphql iql
#[get("/instance/iql")]
pub fn graphiql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/instance")
}
