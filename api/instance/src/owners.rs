use reutils::{AuthToken, reason::Perm, reason};
use reapi::{InputUuid, Response};
use redb::{Database, Instance};

use rocket_contrib::uuid::Uuid;

/*
** Changing instances
*/

///Přidá nového vlastníka
#[patch("/<instance_id>/owners", format = "application/json", data = "<input>", rank = 2)]
pub fn patch(
	instance_id: Uuid,
	input: InputUuid,
	tok: AuthToken<reason::InstanceOwners>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::InstanceOwners::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	if db.read().iter().filter(|(id, _)| id == &*instance_id).count() != 0 {
		db.write().update::<_, Instance, _>(*instance_id, |c| {
			c.map(|mut x| {
				x.owners.push(input.0);
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("instance doesn't exist")
	}
}

///Odebere vlastníka
#[delete(
	"/<instance_id>/owners",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
pub fn delete(
	instance_id: Uuid,
	input: InputUuid,
	tok: AuthToken<reason::InstanceOwners>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::InstanceOwners::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	if let Some(i) = db.read().iter().find(|(id, _)| id == &*instance_id) {
		if i.1.owners.len() == 1 {
			bad_request!("can't remove last owner")
		}

		db.write().update::<_, Instance, _>(*instance_id, |c| {
			c.map(|mut x| {
				x.owners.push(input.0);
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("instance doesn't exist")
	}
}
