//! Modul obsahující endpointy instancí
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;

extern crate redb;
extern crate reutils;
#[macro_use]
extern crate reapi;

use reapi::Response;
use reutils::{AuthToken, reason::Perm, reason};
use redb::{Database, Instance, RInstance, NewEntry, NewEntryPartial};

use rocket::Route;
use rocket_contrib::uuid::Uuid;

/// owner endpoints
pub mod owners;
/// tags endpoints
pub mod tags;
/// graphql
pub mod graphql;

/*
** Base endpoints
*/

///Vrací základní informace o dané instanci
#[get("/instance/<instance_id>", rank = 2)]
pub fn get(instance_id: Uuid, db: Database<Instance>) -> Response<Instance> {
	ok!(db.read().iter().find_map(|(id, x)| if id == *instance_id {
		Some(x)
	} else {
		None
	})?)
}

///Vytvoří novou instanci
#[post("/instance/new", format = "application/json", data = "<input>")]
pub fn post(
	input: RInstance,
	info: AuthToken<reason::InstanceCreate>,
	_db: Database<Instance>,
) -> Response<()> {
	match Instance::create(input).and_modify(|mut x| x.owners = vec![info.id]).save() {
		Ok(_) => ok!(),
		Err(e) => bad_request!(format!("{}", e)),
	}
}

///Smaže instanci a všechna s ní asociovaná data
/// TODO: delete orphan content
#[delete("/instance/<instance_id>", format = "application/json", rank = 3)]
pub fn delete(
	instance_id: Uuid,
	tok: AuthToken<reason::InstanceDelete>,
	mut db: Database<Instance>,
) -> Response<()> {
	if !reason::InstanceDelete::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	match db.write().delete(*instance_id) {
		Ok(_) => ok!(),
		Err(e) => bad_request!(format!("{}", e)),
	}
}

/// returns routes
pub fn routes() -> Vec<Route> {
	routes![
		get,
		post,
		delete,
		graphql::graphql,
		graphql::graphiql,
		graphql::graphql_post,
		owners::patch,
		owners::delete,
		tags::post,
		tags::patch,
		tags::delete
	]
}
