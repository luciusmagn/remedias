use rocket::response::content;

use redb::graphql::{CategoryRoot, CategorySchema, category::CategoryMutations};
use reutils::{reason, AuthToken, auth::JwtContext};

/*
** GraphQL
*/

/// graphql get
#[get("/category?<request>")]
pub fn graphql(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&CategorySchema::new(CategoryRoot, CategoryMutations), &JwtContext {
		jwt: tok,
	})
}

/// graphql post
#[post("/category", data = "<request>")]
pub fn graphql_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&CategorySchema::new(CategoryRoot, CategoryMutations), &JwtContext {
		jwt: tok,
	})
}

/// graphql iql
#[get("/category/iql")]
pub fn graphiql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/category")
}
