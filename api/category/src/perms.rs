use rocket_contrib::uuid::Uuid;

use redb::{Category, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputList};

/*
** Perms management endpoints
*/

/// Přepíše současné modifikátory přístupu dané kategorie
/// POST /<instance_id>/category/<category_id>/perms
#[post(
	"/<instance_id>/category/<category_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
pub fn post(
	instance_id: Uuid,
	category_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Categories>,
	mut db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Category, _>(*category_id, |c| {
			c.map(|mut x| {
				x.perms = input.clone().map(|v| v.0).unwrap_or_default();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}

/// Rozšíří seznam modifikátorů přístupu dané kategorie o dodaný list
/// PATCH /<instance_id>/category/<category_id>/perms
#[patch(
	"/<instance_id>/category/<category_id>/perms",
	format = "application/json",
	data = "<input>"
)]
pub fn patch(
	instance_id: Uuid,
	category_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Categories>,
	mut db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Category, _>(*category_id, |c| {
			c.map(|mut x| {
				x.perms.extend(input.clone().map(|v| v.0).unwrap_or_default());
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}

/// Smaže modifikátory přístupu, chování viz. categories_delete
/// DELETE /<instance_id>/category/<category_id>/perms
#[delete(
	"/<instance_id>/category/<category_id>/perms",
	format = "application/json",
	data = "<input>"
)]
pub fn delete(
	instance_id: Uuid,
	category_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Categories>,
	mut db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Category, _>(*category_id, |c| {
			c.map(|mut x| {
				x.perms.retain(|p| {
					!input.clone().map(|v| v.0).unwrap_or_default().contains(p)
				});
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}
