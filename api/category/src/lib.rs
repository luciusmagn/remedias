//! Modul obsahující endpointy kategorií
#![feature(proc_macro_hygiene, decl_macro)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate uuid;

extern crate redb;
#[macro_use]
extern crate reapi;
extern crate reutils;

use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{RCategory, Category, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::Response;

/// article endpoints
pub mod articles;
/// perm endpoints
pub mod perms;
/// graphql
pub mod graphql;

/*
** Category endpoints
*/

/// Vrací seznam podkategorií dané kategorie
/// GET /<instance_id>/category>/<category_id>/children
#[get("/<instance_id>/category/<category_id>/children", rank = 2)]
pub fn children(
	instance_id: Uuid,
	category_id: Uuid,
	db: Database<Category>,
) -> Response<Vec<uuid::Uuid>> {
	ok!(db
		.read()
		.iter()
		.filter(|(_, x)| x.owner == *instance_id)
		.filter(|(_, x)| x.parent == Some(*category_id))
		.map(|(id, _)| id)
		.collect::<Vec<uuid::Uuid>>())
}

/// Vrací základní údaje o dané kategorie
/// GET /<instance_id>/category/<category_id>
#[get("/<instance_id>/category/<category_id>")]
pub fn get(
	instance_id: Uuid,
	category_id: Uuid,
	db: Database<Category>,
) -> Response<Category> {
	ok!(db.read().get(*category_id).and_then(|x| if x.owner != *instance_id {
		None
	} else {
		Some(x)
	})?)
}

/// Vytvoří novou kategorii
/// POST /<instance_id>/category
#[post("/<instance_id>/category", format = "application/json", data = "<input>")]
pub fn post(
	instance_id: Uuid,
	input: RCategory,
	info: AuthToken<reason::Categories>,
	_db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	Category::create(input).and_modify(|mut x| x.owner = *instance_id).save()?;

	ok!()
}

/// change category
#[patch(
	"/<instance_id>/category/<category_id>",
	format = "application/json",
	data = "<input>"
)]
pub fn patch(
	instance_id: Uuid,
	category_id: Uuid,
	mut db: Database<Category>,
	info: AuthToken<reason::Categories>,
	input: RCategory,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Category, _>(*category_id, |c| {
			c.map(|mut x| {
				x.slug = input.slug.clone();
				x.name = input.name.clone();
				x.parent = input.parent;
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}

/// smaže kategorii s daným UUID
/// DELETE /<instance_id>/category/<category_id>
#[delete("/<instance_id>/category/<category_id>", format = "application/json")]
pub fn delete(
	instance_id: Uuid,
	category_id: Uuid,
	info: AuthToken<reason::Categories>,
	mut db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().delete(*category_id)?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}

/*
** Activity
*/

/// (De)aktivuje kategorii
#[post("/<instance_id>/category/<category_id>/activate", format = "application/json")]
pub fn activate(
	instance_id: Uuid,
	category_id: Uuid,
	info: AuthToken<reason::Categories>,
	mut db: Database<Category>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*category_id && x.owner == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Category, _>(*category_id, |c| {
			c.map(|mut x| {
				x.active = !x.active;
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("category doesn't exist")
	}
}

/// returns routes
pub fn routes() -> Vec<Route> {
	routes![
		children,
		get,
		post,
		patch,
		delete,
		activate,
		perms::post,
		perms::patch,
		perms::delete,
		articles::post,
		articles::delete,
		graphql::graphql,
		graphql::graphiql,
		graphql::graphql_post,
	]
}
