use rocket_contrib::uuid::Uuid;

use redb::{Content, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputList};

/*
** Compound endpoints
*/
use std::str::FromStr;

/// Přiřadí k dané kategorii články
#[post(
	"/<instance_id>/category/<category_id>/articles",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
#[allow(clippy::blocks_in_if_conditions)]
pub fn post(
	instance_id: Uuid,
	category_id: Uuid,
	input: InputList,
	info: AuthToken<reason::Categories>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	let mut db = Database::<Content>::open().unwrap();

	if input
		.0
		.iter()
		.filter_map(|x| match Uuid::from_str(x) {
			Ok(o) => Some(o),
			Err(_) => None,
		})
		.map(|id| {
			db.write().update::<_, Content, _>(*id, |c| {
				c.map(|mut x| {
					if x.instance == *instance_id {
						// TOOD check if actually category exists
						x.categories.insert(*category_id);
					}
					x
				})
			})
		})
		.any(|x| x.is_err())
	{
		ok!()
	} else {
		bad_request!("couldn't add all articles to category")
	}
}

/// Odebere články z dané kategorie
#[delete(
	"/<instance_id>/category/<category_id>/articles",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
#[allow(clippy::blocks_in_if_conditions)]
pub fn delete(
	instance_id: Uuid,
	category_id: Uuid,
	input: InputList,
	info: AuthToken<reason::Categories>,
) -> Response<()> {
	if !reason::Categories::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	let mut db = Database::<Content>::open().unwrap();

	if input
		.0
		.iter()
		.filter_map(|x| match Uuid::from_str(x) {
			Ok(o) => Some(o),
			Err(_) => None,
		})
		.map(|id| {
			db.write().update::<_, Content, _>(*id, |c| {
				c.map(|mut x| {
					if x.instance == *instance_id {
						// TOOD check if actually category exists
						x.categories.retain(|x| x != &*category_id);
					}
					x
				})
			})
		})
		.any(|x| x.is_err())
	{
		ok!()
	} else {
		bad_request!("couldn't add all articles to category")
	}
}
