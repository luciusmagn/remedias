//! Modul obsahující endpointy uživatelů
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate uuid;

extern crate redb;
extern crate reutils;
#[macro_use]
extern crate reapi;

use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{Role, RRole, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::Response;

/// graphql
pub mod graphql;
/// groups
pub mod groups;
/// perms
pub mod perms;

/*
** Role endpoints
*/

/// Vrátí záznam role"
#[get("/<_instance_id>/role/<role_id>")]
pub fn get(_instance_id: Uuid, role_id: Uuid, db: Database<Role>) -> Response<Role> {
	ok!(db.read().get(&*role_id)?)
}

/// Vytvoří novou roli"
#[post("/<instance_id>/role", format = "application/json", data = "<input>")]
#[allow(unused_mut)]
pub fn post(
	instance_id: Uuid,
	input: RRole,
	info: AuthToken<reason::Roles>,
	mut _db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	Role::create(input).and_modify(|mut x| x.instance = *instance_id).save()?;

	ok!()
}

/// Upraví stávající roli komprehensivně"
#[patch(
	"/<instance_id>/role/<role_id>",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
pub fn patch(
	instance_id: Uuid,
	role_id: Uuid,
	input: RRole,
	info: AuthToken<reason::Roles>,
	mut db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*role_id).is_some() {
		db.write().update::<_, Role, _>(*role_id, |c| {
			c.map(|mut x| {
				x.name = input.name.clone();
				x.parent = input.parent;
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("role doesn't exist")
	}
}

/// Smaže roli"
#[delete("/<instance_id>/role/<role_id>", format = "application/json")]
pub fn delete(
	instance_id: Uuid,
	role_id: Uuid,
	info: AuthToken<reason::Roles>,
	mut db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*role_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().delete(*role_id)?;

		ok!()
	} else {
		bad_request!("role doesn't exist")
	}
}

/// fet the fucking routes
pub fn routes() -> Vec<Route> {
	routes![
		get,
		post,
		patch,
		delete,
		perms::post,
		perms::patch,
		perms::delete,
		graphql::role,
		graphql::role_iql,
		graphql::role_post,
		graphql::group,
		graphql::group_iql,
		graphql::group_post,
		groups::get,
		groups::post,
		groups::patch,
		groups::delete,
	]
}
