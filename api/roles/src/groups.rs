use rocket_contrib::uuid::Uuid;

use redb::{Group, RGroup, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::Response;

/*
** Groups
*/

/// get group
#[get("/<instance_id>/group/<id>")]
pub fn get(instance_id: Uuid, id: Uuid, db: Database<Group>) -> Response<Group> {
	ok!(db
		.read()
		.iter()
		.filter_map(|(i, x)| if i == *id && x.instance == *instance_id {
			Some(x)
		} else {
			None
		})
		.next()?)
}

/// create group
#[post("/<instance_id>/group", format = "application/json", data = "<input>")]
pub fn post(
	instance_id: Uuid,
	input: RGroup,
	info: AuthToken<reason::Groups>,
	mut _db: Database<Group>,
) -> Response<()> {
	if !reason::Groups::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	Group::create(input).and_modify(|mut x| x.instance = *instance_id).save()?;

	ok!()
}

/// Upraví stávající skupinu komprehensivně
#[patch(
	"/<instance_id>/group/<group_id>",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
pub fn patch(
	instance_id: Uuid,
	group_id: Uuid,
	input: RGroup,
	info: AuthToken<reason::Groups>,
	mut db: Database<Group>,
) -> Response<()> {
	if !reason::Groups::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*group_id).is_some() {
		db.write().update::<_, Group, _>(*group_id, |c| {
			c.map(|mut x| {
				x.name = input.name.clone();
				x.description = input.description.clone();
				x.role = input.role;
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("group doesn't exist")
	}
}

/// delete group
#[delete("/<instance_id>/group/<group_id>", format = "application/json", rank = 3)]
pub fn delete(
	instance_id: Uuid,
	group_id: Uuid,
	tok: AuthToken<reason::Groups>,
	mut db: Database<Group>,
) -> Response<()> {
	if !reason::Groups::with_id(&tok, &*instance_id) {
		forbidden!("permission denied")
	}

	match db.write().delete(*group_id) {
		Ok(_) => ok!(),
		Err(e) => bad_request!(format!("{}", e)),
	}
}
