use rocket_contrib::uuid::Uuid;

use redb::{Role, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputList};

/*
** Perms
*/

/// Přepíše oprávnění dané role"
#[post(
	"/<instance_id>/role/<role_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn post(
	instance_id: Uuid,
	role_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*role_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Role, _>(*role_id, |c| {
			c.map(|mut x| {
				x.perms = input.clone().map(|v| v.0).unwrap_or_default();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("role doesn't exist")
	}
}

/// Rozšíří oprávnění dané role"
#[patch(
	"/<instance_id>/role/<role_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn patch(
	instance_id: Uuid,
	role_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*role_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Role, _>(*role_id, |c| {
			c.map(|mut x| {
				x.perms.extend(input.clone().map(|v| v.0).unwrap_or_default());
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("role doesn't exist")
	}
}

/// Smaže modifikátory přístupu, chování viz. categories_delete"
#[delete(
	"/<instance_id>/role/<role_id>/perms",
	format = "application/json",
	data = "<input>",
	rank = 5
)]
pub fn delete(
	instance_id: Uuid,
	role_id: Uuid,
	input: Option<InputList>,
	info: AuthToken<reason::Roles>,
	mut db: Database<Role>,
) -> Response<()> {
	if !reason::Roles::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*role_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Role, _>(*role_id, |c| {
			c.map(|mut x| {
				x.perms.retain(|p| {
					!input.clone().map(|v| v.0).unwrap_or_default().contains(p)
				});
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("role doesn't exist")
	}
}
