use redb::graphql::{RoleRoot, RoleSchema, role::RoleMutations};
use redb::graphql::{GroupRoot, GroupSchema, group::GroupMutations};
use reutils::{reason, AuthToken, auth::JwtContext};

use rocket::response::content;

/*
** GraphQL
*/

/// graphql get
#[get("/role?<request>")]
pub fn role(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&RoleSchema::new(RoleRoot, RoleMutations), &JwtContext { jwt: tok })
}

/// graphql post
#[post("/role", data = "<request>")]
pub fn role_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&RoleSchema::new(RoleRoot, RoleMutations), &JwtContext { jwt: tok })
}

/// graphql iql
#[get("/role/iql")]
pub fn role_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/role")
}

/*
** GraphQL group
*/

/// graphql get
#[get("/group?<request>")]
pub fn group(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request
		.execute(&GroupSchema::new(GroupRoot, GroupMutations), &JwtContext { jwt: tok })
}

/// graphql post
#[post("/group", data = "<request>")]
pub fn group_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request
		.execute(&GroupSchema::new(GroupRoot, GroupMutations), &JwtContext { jwt: tok })
}

/// graphql iql
#[get("/group/iql")]
pub fn group_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/group")
}
