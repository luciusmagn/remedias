use redb::graphql::{
	ContentRoot, ContentSchema, content::ContentMutations, AdRoot, AdSchema,
	ad::AdMutations,
};
use reutils::{reason, AuthToken, auth::JwtContext};

use rocket::response::content;

/*
** GraphQL
*/

#[get("/content?<request>")]
pub fn content(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&ContentSchema::new(ContentRoot, ContentMutations), &JwtContext {
		jwt: tok,
	})
}

#[post("/content", data = "<request>")]
pub fn content_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&ContentSchema::new(ContentRoot, ContentMutations), &JwtContext {
		jwt: tok,
	})
}

#[get("/content/iql")]
pub fn content_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/content")
}

/*
** GraphQL Ads
*/

#[get("/ad?<request>")]
pub fn ad(request: juniper_rocket::GraphQLRequest) -> juniper_rocket::GraphQLResponse {
	request.execute(&AdSchema::new(AdRoot, AdMutations), &JwtContext { jwt: None })
}

#[post("/ad", data = "<request>")]
pub fn ad_post(
	request: juniper_rocket::GraphQLRequest,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&AdSchema::new(AdRoot, AdMutations), &JwtContext { jwt: None })
}

#[get("/ad/iql")]
pub fn ad_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/ad")
}
