use rocket_contrib::uuid::Uuid;

use redb::{Ad, RAd, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason};
use reapi::Response;

/*
** Ads
*/

/// Vytvoří nový článek"
#[post("/ad/new", format = "application/json", data = "<input>")]
pub fn post_ad(
	input: RAd,
	info: AuthToken<reason::Ads>,
	mut _db: Database<Ad>,
) -> Response<Ad> {
	Ad::create(input)
		.and_modify(|mut x| {
			x.owner = info.id;
		})
		.save()?;

	ok!()
}

/// Info o dané reklamě"
#[get("/ad/<ad_id>")]
pub fn get_ad(ad_id: Uuid, db: Database<Ad>) -> Response<Ad> {
	ok!(db.read().iter().find_map(|(id, x)| if id == *ad_id { Some(x) } else { None })?)
}

/// Smaže reklamu"
#[delete("/ad/<_ad_id>", format = "application/json")]
pub fn delete_ad(
	_ad_id: Uuid,
	_info: AuthToken<reason::Ads>,
	_db: Database<Ad>,
) -> Response<()> {
	unimplemented!("gay")
}

/// 'Klikne' na reklamu"
#[delete("/<_instance_id>/follow-ad/<_ad_id>")]
pub fn follow_ad(_instance_id: Uuid, _ad_id: Uuid, _db: Database<Ad>) -> Response<()> {
	unimplemented!("gay")
}

/// Vrátí reklamy týkající se určitého tagu"
#[get("/ads/<_tag>/<_amount>")]
pub fn get_ads_by_tag(
	_tag: String,
	_amount: u32,
	_db: Database<Ad>,
) -> Response<Vec<uuid::Uuid>> {
	unimplemented!("gay")
}
