use rocket_contrib::uuid::Uuid;

use redb::{Content, Database};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::{Response, InputUuid};

/*
** Tasks
*/

/// Přidá/změní úkol, ke kterému je článek přiřazený"
#[patch(
	"/<instance_id>/<content_id>/task",
	format = "application/json",
	data = "<input>",
	rank = 3
)]
pub fn patch_id(
	instance_id: Uuid,
	content_id: Uuid,
	input: InputUuid,
	info: AuthToken<reason::Content>,
	mut db: Database<Content>,
) -> Response<()> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*content_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Content, _>(*content_id, |c| {
			c.map(|mut x| {
				x.task_id = Some(input.0);
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("content doesn't exist")
	}
}

/// Odebere úkol od článku"
#[delete("/<instance_id>/<content_id>/task", format = "application/json", rank = 2)]
pub fn delete_id(
	instance_id: Uuid,
	content_id: Uuid,
	info: AuthToken<reason::Content>,
	mut db: Database<Content>,
) -> Response<()> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*content_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Content, _>(*content_id, |c| {
			c.map(|mut x| {
				x.task_id = None;
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("content doesn't exist")
	}
}
