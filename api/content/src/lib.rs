//! Modul obsahující endpointy obsahu
#![feature(proc_macro_hygiene, decl_macro)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate chrono;
extern crate uuid;

extern crate redb;
extern crate reutils;
#[macro_use]
extern crate reapi;
extern crate reutils as util;

use chrono::Utc;
use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{Content, RContent, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::Response;

/// graphql
pub mod graphql;
/// ads
pub mod ads;
/// tasks
pub mod tasks;

/*
** Content creation and deletion
*/

/// Získá informace o daném obsahu"
#[get("/<_instance_id>/<content_id>", rank = 5)]
pub fn get(
	_instance_id: Uuid,
	content_id: Uuid,
	db: Database<Content>,
) -> Response<Content> {
	ok!(db.read().get(*content_id)?)
}

/// Vytvoří nový obsah"
#[post("/<instance_id>/new", format = "application/json", data = "<input>", rank = 2)]
#[allow(unused_mut)] // for soundness reasons
pub fn post(
	instance_id: Uuid,
	input: RContent,
	info: AuthToken<reason::Content>,
	mut _db: Database<Content>,
) -> Response<()> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("permission denied")
	}

	Content::create(input)
		.and_modify(|mut x| {
			x.instance = *instance_id;
			x.author = info.id;
		})
		.save()?;

	ok!()
}

/// Smaže obsah s daným UUID"
#[delete("/<instance_id>/<content_id>", format = "application/json", rank = 4)]
pub fn delete(
	instance_id: Uuid,
	content_id: Uuid,
	info: AuthToken<reason::Content>,
	mut db: Database<Content>,
) -> Response<()> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*content_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().delete(*content_id)?;

		ok!()
	} else {
		bad_request!("content doesn't exist or does not belong to this instance")
	}
}

/// Publikuje daný obsah"
#[post("/<instance_id>/<content_id>/publish", format = "application/json")]
pub fn publish(
	instance_id: Uuid,
	content_id: Uuid,
	info: AuthToken<reason::Content>,
	mut db: Database<Content>,
) -> Response<Content> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().iter().any(|(id, x)| id == *content_id && x.instance == *instance_id) {
		db.write().update::<_, Content, _>(*content_id, |c| {
			c.map(|mut x| {
				x.published = true;
				x.published_at = Some(Utc::now().to_rfc3339());
				x.updated_at = Utc::now().to_rfc3339();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("content doesn't exist")
	}
}

/// change category
#[patch("/<instance_id>/<content_id>", format = "application/json", data = "<input>")]
pub fn patch(
	instance_id: Uuid,
	content_id: Uuid,
	mut db: Database<Content>,
	info: AuthToken<reason::Content>,
	input: RContent,
) -> Response<()> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*content_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().update::<_, Content, _>(*content_id, |c| {
			c.map(|mut x| {
				x.name = input.name.clone();
				x.contents = input.contents.clone();
				x.tags = input.tags.clone();
				x.categories = input.categories.clone();
				x.location = input.location.clone();
				x.description = input.description.clone();
				x.image_url = input.image_url.clone();
				x.task_id = input.task_id;
				x.group = input.group;
				x.slug = input.slug.clone();
				x.publish_at = input.publish_at.clone();
				x.updated_at = Utc::now().to_rfc3339();
				if x.published && input.publish_at.is_some() {
					x.published_at = input.publish_at.clone();
				}
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("content doesn't exist")
	}
}

/// Zruší publikaci daného obsahu"
#[post("/<instance_id>/<content_id>/unpublish", format = "application/json")]
pub fn unpublish(
	instance_id: Uuid,
	content_id: Uuid,
	info: AuthToken<reason::Content>,
	mut db: Database<Content>,
) -> Response<Content> {
	if !reason::Content::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().iter().any(|(id, x)| id == *content_id && x.instance == *instance_id) {
		db.write().update::<_, Content, _>(*content_id, |c| {
			c.map(|mut x| {
				x.published = false;
				x.published_at = Some(Utc::now().to_rfc3339());
				x.updated_at = Utc::now().to_rfc3339();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("content doesn't exist")
	}
}

pub fn routes() -> Vec<Route> {
	routes![
		get,
		post,
		patch,
		delete,
		publish,
		unpublish,
		tasks::patch_id,
		tasks::delete_id,
		graphql::content,
		graphql::content_iql,
		graphql::content_post,
		graphql::ad,
		graphql::ad_iql,
		graphql::ad_post,
	]
}
