//! Modul obsahující endpointy tasků
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate uuid;

extern crate redb;
extern crate reutils;
#[macro_use]
extern crate reapi;

use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{RTask, Task, Database, NewEntry, NewEntryPartial};
use reutils::{AuthToken, reason, reason::Perm};
use reapi::Response;

/// graphql
pub mod graphql;

/*
** Task endpoints
*/

/// Vrací task
#[get("/<_instance_id>/task/<task_id>", rank = 2)]
pub fn get(_instance_id: Uuid, task_id: Uuid, db: Database<Task>) -> Response<Task> {
	ok!(db.read().get(*task_id)?)
}

/// Vytvoří nový task
#[post("/<instance_id>/task", format = "application/json", data = "<input>", rank = 2)]
pub fn post(
	instance_id: Uuid,
	input: RTask,
	info: AuthToken<reason::Tasks>,
	mut _db: Database<Task>,
) -> Response<()> {
	if !reason::Tasks::with_id(&info, &*instance_id) {
		forbidden!("you don't have the require permissions")
	}

	Task::create(input)
		.and_modify(|mut x| {
			x.instance = *instance_id;
			x.creator_id = info.id;
		})
		.save()?;

	ok!()
}

/// Upraví task
#[patch(
	"/<instance_id>/task/<task_id>",
	format = "application/json",
	data = "<input>",
	rank = 2
)]
pub fn patch(
	instance_id: Uuid,
	task_id: Uuid,
	input: RTask,
	info: AuthToken<reason::Tasks>,
	mut db: Database<Task>,
) -> Response<()> {
	if !reason::Tasks::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*task_id).is_some() {
		db.write().update::<_, Task, _>(*task_id, |c| {
			c.map(|mut x| {
				x.title = input.title.clone();
				x.content = input.content.clone();
				x.category = input.category;
				x.priority = input.priority.clone();
				x.due_at = input.due_at;
				x.assignee_id = input.assignee_id;
				x.reviewer_id = input.reviewer_id;
				x.after_completed = input.after_completed.clone();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("task doesn't exist")
	}
}

/// Smaže task
#[delete("/<instance_id>/task/<task_id>", format = "application/json", rank = 6)]
pub fn delete(
	instance_id: Uuid,
	task_id: Uuid,
	info: AuthToken<reason::Tasks>,
	mut db: Database<Task>,
) -> Response<()> {
	if !reason::Tasks::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db
		.read()
		.iter()
		.filter(|(id, x)| id == &*task_id && x.instance == *instance_id)
		.count() != 0
	{
		db.write().delete(*task_id)?;

		ok!()
	} else {
		bad_request!("task doesn't exist")
	}
}

// todo smarter
/// Posune stav tasku
#[patch("/<instance_id>/task/<task_id>/state/<new_state>", format = "application/json")]
pub fn change_state(
	instance_id: Uuid,
	task_id: Uuid,
	new_state: String,
	info: AuthToken<reason::Tasks>,
	mut db: Database<Task>,
) -> Response<()> {
	if !reason::Tasks::with_id(&info, &*instance_id) {
		forbidden!("you don't have the required permissions")
	}

	if db.read().get(&*task_id).is_some() {
		db.write().update::<_, Task, _>(*task_id, |c| {
			c.map(|mut x| {
				x.state = new_state.clone();
				x
			})
		})?;

		ok!()
	} else {
		bad_request!("task doesn't exist")
	}
}

/// get routes
pub fn routes() -> Vec<Route> {
	routes![
		get,
		post,
		patch,
		delete,
		change_state,
		graphql::task,
		graphql::task_iql,
		graphql::task_post
	]
}
