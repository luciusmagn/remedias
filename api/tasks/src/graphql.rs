use redb::graphql::{TaskRoot, TaskSchema, task::TaskMutations};
use reutils::{reason, AuthToken, auth::JwtContext};

use rocket::response::content;

/*
** GraphQL
*/

/// graphql get
#[get("/task?<request>")]
pub fn task(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&TaskSchema::new(TaskRoot, TaskMutations), &JwtContext { jwt: tok })
}

/// graphql post
#[post("/task", data = "<request>")]
pub fn task_post(
	request: juniper_rocket::GraphQLRequest,
	tok: Option<AuthToken<reason::Me>>,
) -> juniper_rocket::GraphQLResponse {
	request.execute(&TaskSchema::new(TaskRoot, TaskMutations), &JwtContext { jwt: tok })
}

/// graphql iql
#[get("/task/iql")]
pub fn task_iql() -> content::Html<String> {
	juniper_rocket::graphiql_source("/task")
}
