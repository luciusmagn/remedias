//! Modul obsahující veškeré endpointy pro vyhledávání a filtrování
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs)]

extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
extern crate uuid;

extern crate redb;
extern crate reutils;
#[macro_use]
extern crate reapi;

use rocket::Route;
use rocket_contrib::uuid::Uuid;

use redb::{
	Task, User, Content, Category, Instance, Role, Database,
	graphql::{UserRoot, TaskRoot, RoleRoot, ContentRoot, CategoryRoot, InstanceRoot},
};
use reapi::{Response};

/*
** Content
*/
/// Vrací všechen obsah dané instance

/// Asi nebudeme chtít aby velké instance toto volaly často,
/// byť Remedias by to měl zvládnout"
#[get("/<instance_id>/content", rank = 3)]
pub fn content_all(instance_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	let mut entries: Vec<Content> = ContentRoot::content_with_instance(*instance_id);

	entries.sort_by(|b, a| a.created_at.cmp(&b.created_at));

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Podle aktualizace
#[get("/<instance_id>/content/by-updated")]
pub fn content_updated(instance_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	let mut entries = ContentRoot::content_with_instance(*instance_id);

	entries.sort_by(|b, a| a.updated_at.cmp(&b.updated_at));

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Vrací všechny 'koncepty'"
#[get("/<instance_id>/content/unpublished")]
pub fn content_unpublished(instance_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	let mut entries = ContentRoot::content_with_instance_published(*instance_id, false);

	entries.sort_by(|b, a| a.created_at.clone().cmp(&b.clone().created_at));

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Vrací <amount> náhodných článků instance <instance_id>"
#[get("/<instance_id>/content/random/<amount>")]
pub fn content_random(instance_id: Uuid, amount: u16) -> Response<Vec<uuid::Uuid>> {
	ok!(ContentRoot::content_with_instance_random(*instance_id, amount as i32)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací <amount> článků instance <instance_id> řazených dle data"
#[get("/<instance_id>/content/by-date/<ord>/<amount>")]
pub fn content_date(
	instance_id: Uuid,
	ord: String,
	amount: u16,
) -> Response<Vec<uuid::Uuid>> {
	ok!(ContentRoot::content_with_instance_date(*instance_id, ord, amount as i32)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací <amount> článků instance <instance_id> řazených dle data počínaje Xtým článkem"
#[get("/<instance_id>/content/by-date/<ord>/<start>/<amount>")]
pub fn content_date_offset(
	instance_id: Uuid,
	ord: String,
	start: u16,
	amount: u16,
) -> Response<Vec<uuid::Uuid>> {
	ok!(ContentRoot::content_with_instance_date_offset(
		*instance_id,
		ord,
		start as i32,
		amount as i32
	)
	.iter()
	.map(|x| x.id)
	.collect::<Vec<_>>())
}

/// Vrací všechny články s daným tagem"
#[get("/<instance_id>/content/by-tag/<tag>")]
pub fn content_tag(instance_id: Uuid, tag: String) -> Response<Vec<uuid::Uuid>> {
	let mut entries = ContentRoot::content_with_instance_tag(*instance_id, tag);

	entries.sort_by(|b, a| {
		a.published_at
			.clone()
			.unwrap_or_default()
			.cmp(&b.clone().published_at.unwrap_or_default())
	});

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Vrací článek s daným slugem"
#[get("/<instance_id>/content/by-slug/<slug>")]
pub fn content_slug(instance_id: Uuid, slug: String) -> Response<Option<Content>> {
	ok!(ContentRoot::content_with_slug(*instance_id, slug))
}

/// Vrací všechny články zařazené do dané kategorie"
#[get("/<_instance_id>/content/by-category/<category_id>")]
pub fn content_category(
	_instance_id: Uuid,
	category_id: Uuid,
) -> Response<Vec<uuid::Uuid>> {
	let mut entries = ContentRoot::content_with_category(*category_id);

	entries.sort_by(|b, a| {
		a.published_at
			.clone()
			.unwrap_or_default()
			.cmp(&b.clone().published_at.unwrap_or_default())
	});

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Vrací všechny články zařazené do dané kategorie a jejích podkategorií"
#[get("/<_instance_id>/content/by-category-children/<category_id>")]
pub fn content_category_children(
	_instance_id: Uuid,
	category_id: Uuid,
) -> Response<Vec<uuid::Uuid>> {
	let mut entries = ContentRoot::content_with_category_children(*category_id);

	entries.sort_by(|b, a| {
		a.published_at
			.clone()
			.unwrap_or_default()
			.cmp(&b.clone().published_at.unwrap_or_default())
	});

	ok!(entries.iter().map(|x| x.id).collect::<Vec<uuid::Uuid>>())
}

/// Vrací všechny články patřící k danému tasku"
#[get("/<_instance_id>/content/by-task/<task_id>")]
pub fn content_task(_instance_id: Uuid, task_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(TaskRoot::task_with_id(*task_id)?
		.content_list()
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/*
** Category
*/

/// Vrací kategorie, jejichž rodičem je kategorie `parentid`"
#[get("/category/by-parent/<parent_id>")]
pub fn category_by_parent(parent_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(CategoryRoot::category_with_parent(*parent_id)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací kategorie vytvořené pod instancí s ID `owner_id`"
#[get("/category/by-owner/<owner_id>")]
pub fn category_by_owner(owner_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(CategoryRoot::category_with_instance(*owner_id)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací kategorie s jménem, které obsahují string `name`"
#[get("/category/by-name/<name>")]
pub fn category_by_name(name: String) -> Response<Vec<uuid::Uuid>> {
	ok!(CategoryRoot::category_with_name(name, None)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací kategorie, jejichž slug obsahuje string `slug`"
#[get("/category/by-slug/<slug>")]
pub fn category_by_slug(slug: String) -> Response<Vec<uuid::Uuid>> {
	ok!(CategoryRoot::category_with_slug(slug, None)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací kategorie dané instance, jež mají stav určení boolem `is_active`"
#[get("/category/by-activity/<owner_id>/<is_active>")]
pub fn category_inactive(owner_id: Uuid, is_active: bool) -> Response<Vec<uuid::Uuid>> {
	ok!(CategoryRoot::category_inactive(*owner_id, is_active)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/*
** Instance
*/

/// Vrací všechny instance
#[get("/instance/all")]
pub fn instance_all() -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_all().iter().map(|x| x.id).collect::<Vec<_>>())
}

/// Vrací instance, jejichž doména obsahuje string `domain`"
#[get("/instance/by-domain/<domain>")]
pub fn instance_by_domain(domain: String) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_with_domain(domain)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací náhodné instance"
#[get("/instance/random/<amount>")]
pub fn instance_random(amount: u16) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_random(amount as i32)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací instance, jejichž pole vlastníků obsahuje `owner_id`"
#[get("/instance/by-owner/<owner_id>")]
pub fn instance_by_owner(owner_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_with_owner(*owner_id)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací instance, jejichž jméno obsahuje string `name`"
#[get("/instance/by-name/<name>")]
pub fn instance_by_name(name: String) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_with_name(name, None)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací instance, jež mají tag `tag`"
#[get("/instance/by-tag/<tag>")]
pub fn instance_by_tag(tag: String) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_with_tag(tag).iter().map(|x| x.id).collect::<Vec<_>>())
}

/*
** User
*/

/// Vrací uživatele - adminy  k dané instanci"
#[get("/user/by-instance-admin/<instance_id>")]
pub fn user_by_instance(instance_id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(InstanceRoot::instance_with_id(*instance_id)?
		.admins()
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací uživatele, jejichž čitelné jméno obsahuje string `display`"
#[get("/user/by-display/<display>")]
pub fn user_by_display(display: String) -> Response<Vec<uuid::Uuid>> {
	ok!(UserRoot::user_with_display(display, None)
		.iter()
		.map(|x| x.id)
		.collect::<Vec<_>>())
}

/// Vrací uživatele s emailem `email`"
#[get("/user/by-email/<email>")]
pub fn user_by_email(email: String) -> Response<Option<User>> {
	ok!(UserRoot::user_with_email(email))
}

/// Vrací uživatele, jejichž přihlašovací jméno obsahuje string `nick`"
#[get("/user/by-nick/<nick>")]
pub fn user_by_nick(nick: String) -> Response<Vec<uuid::Uuid>> {
	ok!(UserRoot::user_with_nick(nick, None).iter().map(|x| x.id).collect::<Vec<_>>())
}

/*
** Role
*/

/// returns all roles for an instance
#[get("/role/by-instance/<id>")]
pub fn role_by_instance(id: Uuid) -> Response<Vec<uuid::Uuid>> {
	ok!(RoleRoot::role_with_instance(*id).iter().map(|x| x.id).collect::<Vec<_>>())
}

/*
** Util
*/

/// identifiuke objekt
#[get("/util/identify/<id>")]
pub fn identify(
	id: Uuid,
	content: Database<Content>,
	tasks: Database<Task>,
	users: Database<User>,
	roles: Database<Role>,
	categories: Database<Category>,
	instances: Database<Instance>,
) -> Response<String> {
	if content.read().get(*id).is_some() {
		return ok!("content".into());
	}
	if tasks.read().get(*id).is_some() {
		return ok!("task".into());
	}
	if users.read().get(*id).is_some() {
		return ok!("user".into());
	}
	if roles.read().get(*id).is_some() {
		return ok!("role".into());
	}
	if instances.read().get(*id).is_some() {
		return ok!("instance".into());
	}
	if categories.read().get(*id).is_some() {
		return ok!("categorie".into());
	}

	ok!("unknown".into())
}

/// returns all perms that exist in Remedias
#[get("/util/all-perms")]
pub fn all_perms() -> Response<Vec<(String, bool)>> {
	use redb::auth::Reason::*;
	ok!(vec![
		(TagsGlobal.to_string(), TagsGlobal.is_global()),
		(Tags.to_string(), Tags.is_global()),
		(ContentCategories.to_string(), ContentCategories.is_global()),
		(Categories.to_string(), Categories.is_global()),
		(InstancePerms.to_string(), InstancePerms.is_global()),
		(InstanceDelete.to_string(), InstanceDelete.is_global()),
		(InstanceOwners.to_string(), InstanceOwners.is_global()),
		(InstanceName.to_string(), InstanceName.is_global()),
		(InstanceCreate.to_string(), InstanceCreate.is_global()),
		(UserDeleteLocal.to_string(), UserDeleteLocal.is_global()),
		(UserDeleteGlobal.to_string(), UserDeleteGlobal.is_global()),
		(ICanDoWhatIWant.to_string(), ICanDoWhatIWant.is_global()),
		(UserModify.to_string(), UserModify.is_global()),
		(Confirm.to_string(), Confirm.is_global()),
		(Content.to_string(), Content.is_global()),
		(Page.to_string(), Page.is_global()),
		(Map.to_string(), Map.is_global()),
		(Ads.to_string(), Ads.is_global()),
		(Me.to_string(), Me.is_global()),
		(Tasks.to_string(), Tasks.is_global()),
		(Roles.to_string(), Roles.is_global()),
	])
}

/// returns routes
pub fn routes() -> Vec<Route> {
	routes![
		content_all,
		content_updated,
		content_unpublished,
		content_random,
		content_date,
		content_date_offset,
		content_tag,
		content_slug,
		content_category,
		content_category_children,
		content_task,
		category_by_parent,
		category_by_owner,
		category_by_name,
		category_by_slug,
		category_inactive,
		instance_all,
		instance_by_domain,
		instance_random,
		instance_by_name,
		instance_by_owner,
		instance_by_tag,
		user_by_instance,
		user_by_display,
		user_by_email,
		user_by_nick,
		role_by_instance,
		identify,
		all_perms,
	]
}
