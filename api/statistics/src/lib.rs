//! statistics
//!
//! Modul obsahující veškeré endpointy pro statistiky
#![feature(proc_macro_hygiene, decl_macro)]
#![deny(missing_docs, unused_extern_crates)]

#[macro_use]
extern crate rocket;

extern crate redb;
#[macro_use]
extern crate reapi;

use rocket::Route;

use reapi::Response;
use redb::{Database, Category, Instance, Content, User};

/*
** County bois
*/

/// Vrací počet všech existujících obsahů platformy
#[get("/count/content")]
pub fn content(db: Database<Content>) -> Response<usize> {
	ok!(db.read().iter().count())
}

/// Vrací počet všech existujících uživatelů platformy
#[get("/count/users")]
pub fn users(db: Database<User>) -> Response<usize> {
	ok!(db.read().iter().count())
}

/// Vrací počet všech ověřených uživatelů platformy
#[get("/count/confirmed")]
pub fn confirmed(db: Database<User>) -> Response<usize> {
	ok!(db.read().iter().filter(|(_, x)| x.confirmed).count())
}

/// Vrací počet všech instancí na platformě
#[get("/count/instances")]
pub fn instances(db: Database<Instance>) -> Response<usize> {
	ok!(db.read().iter().count())
}

/// Vrací počet všech existujících uživatelů platformy
#[get("/count/categories")]
pub fn categories(db: Database<Category>) -> Response<usize> {
	ok!(db.read().iter().count())
}

/// returns routes
pub fn routes() -> Vec<Route> {
	routes![users, content, confirmed, instances, categories]
}
