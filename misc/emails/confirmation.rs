/// Předmět konfirmačního emailu
pub const SUBJECT: &str = "[Meedias] Potvrzení emailové adresy";
/// Obsah konfirmačního emailu
pub const MESSAGE: &str =
// První řádek neměnit, to lomítko tam je úmyslně.
"\
Vítejte v systému Meedias, jménem celého našeho týmu doufáme, že náš produkt splní vaše očekávání a pomůže vám převést vaše ideje ve skutečnost.


Než začnete Meedias používat, je nutné potvrdit vaší emailovou adresu kliknutím na následující odkaz:
{{link}}


V případě jakýchkoliv dotazů se nebojte na nás obrátit..

Hodně štěstí,
Tým Meedias
";
