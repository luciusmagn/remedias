/// Předmět zprávy
pub const SUBJECT: &str = "[Meedias] Změna emailu asociovaného k účtu";
/// Obsah konfirmačního emailu
pub const MESSAGE: &str =
// První řádek neměnit, to lomítko tam je úmyslně.
"\
Právě došlo ke změně emailu asociovaného k Meedias účtu \"{}\" z -{}- na -{}-.
Pokuď se domníváte, že se jedná o chybu, odpovězte prosím na tento email.

Kontrolní ID: {}
";
